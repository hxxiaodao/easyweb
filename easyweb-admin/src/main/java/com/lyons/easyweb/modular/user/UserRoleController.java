package com.lyons.easyweb.modular.user;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.modular.user.pojo.UserRole;
import com.lyons.easyweb.modular.user.service.UserRoleService;

/**
 * <p>
 * 用户与角色关系 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/userRole")
public class UserRoleController {

	@Autowired
	private UserRoleService userRoleService;

	// 保存用户选中的角色信息
	@RequestMapping("/saveUserRole")
	public LayuiPageInfo saveUserRole(String userid, String[] select) {

		userRoleService.saveUserRole(userid, select);
		return LayuiPageInfo.ok("用户角色添加成功 !");
	}
	
	// 查询用户已经拥有的角色
	@RequestMapping("/querUserRoleByUserId")
	public LayuiPageInfo querUserRoleByUserId(String userId) {
		
		List<Map<String, String>> list = userRoleService.querUserRoleByUserId(userId);
		return LayuiPageInfo.ok(list);
	}
	

	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "userId", required = false) String userId, 
			@RequestParam(value = "roleId", required = false) String roleId, 
			@RequestParam(value = "page", defaultValue = "1") Integer current, 
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<UserRole> page = new Page<UserRole>(current, limit);

		QueryWrapper<UserRole> queryWrUserRoleer = new QueryWrapper<UserRole>();

		if (!ObjectUtils.isEmpty(userId)) {
			queryWrUserRoleer.lambda().like(UserRole::getUserId, userId);
		}
		if (!ObjectUtils.isEmpty(roleId)) {
			queryWrUserRoleer.lambda().like(UserRole::getRoleId, roleId);
		}

		queryWrUserRoleer.lambda().orderByDesc(UserRole::getCreateTime);

		IPage<UserRole> ipage = userRoleService.page(page, queryWrUserRoleer);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid UserRole UserRole) {

		userRoleService.saveOrUpdate(UserRole);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(userRoleService.getById(id));
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		userRoleService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		userRoleService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		userRoleService.remove(new QueryWrapper<UserRole>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}

}
