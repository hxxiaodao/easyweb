package com.lyons.easyweb.modular.timer;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.modular.timer.pojo.Timers;
import com.lyons.easyweb.modular.timer.service.TimersService;

/**
 * <p>
 * 定时任务 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-12
 */
@RestController
@RequestMapping("/sys/timers")
public class TimersController {

	@Autowired
	private TimersService timersService;

	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(@RequestParam(value = "timerName", required = false) String timerName, @RequestParam(value = "jobStatus", required = false) String jobStatus, @RequestParam(value = "page", defaultValue = "1") Integer current,
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<Timers> page = new Page<Timers>(current, limit);

		QueryWrapper<Timers> queryWrTimerser = new QueryWrapper<Timers>();

		if (!ObjectUtils.isEmpty(timerName)) {
			queryWrTimerser.lambda().like(Timers::getTimerName, timerName);
		}
		if (!ObjectUtils.isEmpty(jobStatus)) {
			queryWrTimerser.lambda().eq(Timers::getJobStatus, jobStatus);
		}

		queryWrTimerser.lambda().orderByDesc(Timers::getCreateTime);

		IPage<Timers> ipage = timersService.page(page, queryWrTimerser);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	@GetMapping("/listTimersClass")
	public LayuiPageInfo listTimersClass() {

		List<String> list = timersService.getActionClasses();
		return LayuiPageInfo.ok(list);
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid Timers timers) {

		timersService.insertOrUpdate(timers);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(timersService.getById(id));
	}

	// 修改状态
	@RequestMapping("/status")
	public LayuiPageInfo status(String id, boolean status) {

		boolean result = timersService.status(id, status);
		if (result) {
			
			return LayuiPageInfo.ok("定时任务启动成功 !");
		}
		return LayuiPageInfo.ok("定时任务关闭成功 !");
	}

	// 查询所有（指定列）
	@RequestMapping("/listAll")
	public LayuiPageInfo listAll() {

		List<Timers> list = timersService.list(new QueryWrapper<Timers>().lambda().select(Timers::getId, Timers::getTimerName));
		return LayuiPageInfo.ok(list);
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		timersService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		timersService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		timersService.remove(new QueryWrapper<Timers>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}
	
}
