package com.lyons.easyweb.modular.pos;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.exception.BaseResponseCode;
import com.lyons.easyweb.exception.BusinessException;
import com.lyons.easyweb.modular.pos.pojo.Pos;
import com.lyons.easyweb.modular.pos.service.PosService;

/**
 * <p>
 * 系统职位表 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/pos")
public class PosController {
	
	
	
	@Autowired
	private PosService posService;
	
	
	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "code", required = false) String code, 
			@RequestParam(value = "page", defaultValue = "1") Integer current,
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<Pos> page = new Page<Pos>(current, limit);

		QueryWrapper<Pos> queryWrPoser = new QueryWrapper<Pos>();
		
		if (!ObjectUtils.isEmpty(name)) {
			queryWrPoser.lambda().like(Pos::getName, name).or().like(Pos::getId, name);
		}
		if (!ObjectUtils.isEmpty(code)) {
			queryWrPoser.lambda().like(Pos::getCode, code);
		}
		
		queryWrPoser.lambda().orderByDesc(Pos::getSort).orderByDesc(Pos::getCreateTime);

		IPage<Pos> ipage = posService.page(page, queryWrPoser);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid Pos Pos) {

		posService.saveOrUpdate(Pos);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(posService.getById(id));
	}

	// 修改状态
	@RequestMapping("/status")
	public LayuiPageInfo status(String id, boolean status) {

		Pos bean = posService.getById(id);
		if (ObjectUtils.isEmpty(bean)) {
			throw new BusinessException(BaseResponseCode.DATA_ID_EMPTY);
		}
		if (status) {
			bean.setStatus(1);
		} else {
			bean.setStatus(0);
		}
		posService.updateById(bean);
		return LayuiPageInfo.ok("状态修改成功 !");
	}

	// 查询所有（指定列）
	@RequestMapping("/listAll")
	public LayuiPageInfo listAll() {

		List<Pos> list = posService.list(new QueryWrapper<Pos>().lambda().select(Pos::getId, Pos::getName));
		return LayuiPageInfo.ok(list);
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		posService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		posService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		posService.remove(new QueryWrapper<Pos>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}
	
	
}
