package com.lyons.easyweb.modular.log;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.modular.log.pojo.ErrorLog;
import com.lyons.easyweb.modular.log.service.ErrorLogService;

/**
 * <p>
 * 错误信息 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/log/error")
public class ErrorLogController {
	

	@Autowired
	private ErrorLogService errorLogService;
	
	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "device", required = false) String device, 
			@RequestParam(value = "className", required = false) String className, 
			@RequestParam(value = "page", defaultValue = "1") Integer current,
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {
	
		IPage<ErrorLog> page = new Page<ErrorLog>(current, limit);
	
		QueryWrapper<ErrorLog> queryWrErrorLoger = new QueryWrapper<ErrorLog>();
		
		if (!ObjectUtils.isEmpty(device)) {
			queryWrErrorLoger.lambda().like(ErrorLog::getDevice, device);
		}
		if (!ObjectUtils.isEmpty(className)) {
			queryWrErrorLoger.lambda().like(ErrorLog::getClassName, className);
		}
		
		queryWrErrorLoger.lambda().orderByDesc(ErrorLog::getCreateTime);
	
		IPage<ErrorLog> ipage = errorLogService.page(page, queryWrErrorLoger);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}
	
	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid ErrorLog ErrorLog) {
	
		errorLogService.saveOrUpdate(ErrorLog);
		return LayuiPageInfo.ok("数据保存成功 !");
	}
	
	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(errorLogService.getById(id));
	}
	
	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {
	
		errorLogService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}
	
	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {
	
		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());
	
		errorLogService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}
	
	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {
	
		errorLogService.remove(new QueryWrapper<ErrorLog>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}
}
