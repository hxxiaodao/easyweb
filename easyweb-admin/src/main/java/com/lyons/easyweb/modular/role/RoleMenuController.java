package com.lyons.easyweb.modular.role;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.modular.role.pojo.RoleMenu;
import com.lyons.easyweb.modular.role.service.RoleMenuService;

/**
 * <p>
 * 角色与菜单对应关系 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/role/menu")
public class RoleMenuController {
	
	
	
	@Autowired
	private RoleMenuService roleMenuService;
	
	
	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "menuId", required = false) String menuId, 
			@RequestParam(value = "roleId", required = false) String roleId, 
			@RequestParam(value = "page", defaultValue = "1") Integer current,
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<RoleMenu> page = new Page<RoleMenu>(current, limit);

		QueryWrapper<RoleMenu> queryWrRoleMenuer = new QueryWrapper<RoleMenu>();
		
		if (!ObjectUtils.isEmpty(roleId)) {
			queryWrRoleMenuer.lambda().eq(RoleMenu::getRoleId, roleId);
		}
		if (!ObjectUtils.isEmpty(menuId)) {
			queryWrRoleMenuer.lambda().like(RoleMenu::getMenuId, menuId);
		}
		
		queryWrRoleMenuer.lambda().orderByDesc(RoleMenu::getCreateTime);

		IPage<RoleMenu> ipage = roleMenuService.page(page, queryWrRoleMenuer);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(RoleMenu roleMenu) {

		roleMenuService.insertRoleMenu(roleMenu);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(roleMenuService.getById(id));
	}


	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		roleMenuService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		roleMenuService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		roleMenuService.remove(new QueryWrapper<RoleMenu>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}

}
