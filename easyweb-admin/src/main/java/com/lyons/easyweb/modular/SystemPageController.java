package com.lyons.easyweb.modular;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.lyons.easyweb.modular.setting.pojo.Setting;
import com.lyons.easyweb.modular.setting.service.SettingService;

@Controller
public class SystemPageController {

	@Autowired
	private SettingService settingService;

	// 登陆页面
	@GetMapping({ "/admin/login/{date}","/page/login.html"})
	public String loginPage(@PathVariable("date") String date, HttpSession session) {
		
		System.out.println("================>" + date);
		String nowData = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
		if (!date.equals(nowData)) {
			return null;
		}

		Setting setting = settingService.querySettingInfo();
		session.setAttribute("title", setting.getTitle());
		session.setAttribute("sitename", setting.getSitename());

		return "/page/login.html";
	}

	// 首页
	@GetMapping("/page/index.html")
	public String indexPage() {

		return "/page/index.html";
	}

	@GetMapping("/page/system/setting.html")
	public String systemSetting() {
		// 系统设置
		return "/page/modular/system/other/setting.html";
	}

	@GetMapping("/page/welcome-1.html")
	public String welcome() {

		return "page/modular/system/other/welcome-1.html";
	}

	@GetMapping("/page/welcome-2.html")
	public String welcome2() {

		return "page/modular/system/other/welcome-2.html";
	}

	@GetMapping("/page/welcome-3.html")
	public String welcome3() {

		return "page/modular/system/other/welcome-3.html";
	}

	// 桌面
	@GetMapping("/page/welcome.html")
	public String welcomePage() {

		return "/page/modular/system/other/welcome.html";
	}

	// 用户设置
	@GetMapping("/page/user-setting.html")
	public String userSetting() {

		return "/page/modular/system/user/user-setting.html";
	}

	// 用户修改密码
	@GetMapping("/page/user-password.html")
	public String userPassword() {

		return "/page/modular/system/user/user-password.html";
	}

	@GetMapping("/page/other/workplace/index")
	public String workplacePage() {

		return "/page/modular/system/other/workplace.html";
	}

	@GetMapping("/page/other/dashboard/index")
	public String dashboardPage() {

		return "/page/modular/system/other/dashboard.html";
	}

	// = = = = = = = = = = = 系统应用 - - - 权限管理 = = = = = = = = = =

	@GetMapping("/page/system/app/index.html")
	public String systemApp() {
		// 应用管理
		return "/page/modular/system/app/index.html";
	}

	@GetMapping("/page/system/app/form.html")
	public String systemAppForm() {
		// 应用管理---编辑
		return "/page/modular/system/app/form.html";
	}

	@GetMapping("/page/system/menu/index.html")
	public String sysMenuIndex() {
		// 菜单管理
		return "/page/modular/system/menu/index.html";
	}

	@GetMapping("/page/system/menu/form.html")
	public String sysMenuForm() {
		// 菜单管理---编辑
		return "/page/modular/system/menu/form.html";
	}

	@GetMapping("/page/system/role/index.html")
	public String sysRoleIndex() {
		// 角色管理
		return "/page/modular/system/role/index.html";
	}

	@GetMapping("/page/system/role/form.html")
	public String sysRoleForm() {
		// 角色管理---编辑
		return "/page/modular/system/role/form.html";
	}

	@GetMapping("/sys/system/role/menu.html")
	public String sysRoleMenu() {
		// 角色管理 --- 角色与菜单管理
		return "/page/modular/system/role/menu.html";
	}

	@GetMapping("/sys/system/role/dataRange.html")
	public String sysRoleDataRange() {
		// 角色管理 --- 角色数据方位
		return "/page/modular/system/role/dataRange.html";
	}

	@GetMapping("/page/system/user/index.html")
	public String sysUserIndex() {
		// 用户管理
		return "/page/modular/system/user/index.html";
	}

	@GetMapping("/page/system/user/form.html")
	public String sysUserForm() {
		// 用户管理---编辑
		return "/page/modular/system/user/form.html";
	}

	@GetMapping("/sys/system/user/role.html")
	public String sysUserRole() {
		// 用户角色编辑
		return "/page/modular/system/user/role.html";
	}

	@GetMapping("/page/system/org/index.html")
	public String sysOrgIndex() {
		// 机构管理
		return "/page/modular/system/org/index.html";
	}

	@GetMapping("/page/system/org/form.html")
	public String sysOrgForm() {
		// 机构管理---编辑
		return "/page/modular/system/org/form.html";
	}

	@GetMapping("/page/system/dept/index.html")
	public String sysDeptIndex() {
		// 部门管理
		return "/page/modular/system/dept/index.html";
	}

	@GetMapping("/page/system/dept/form.html")
	public String sysDeptForm() {
		// 部门管理---编辑
		return "/page/modular/system/dept/form.html";
	}

	@GetMapping("/page/system/pos/index.html")
	public String sysPosIndex() {
		// 职位管理
		return "/page/modular/system/pos/index.html";
	}

	@GetMapping("/page/system/pos/form.html")
	public String sysPosForm() {
		// 职位管理---编辑
		return "/page/modular/system/pos/form.html";
	}

	@GetMapping("/page/system/config/index.html")
	public String sysConfigIndex() {
		// 系统配置
		return "/page/modular/system/config/index.html";
	}

	@GetMapping("/page/system/config/form.html")
	public String sysConfigForm() {
		// 系统配置---编辑
		return "/page/modular/system/config/form.html";
	}

	@GetMapping("/page/system/dict/index.html")
	public String sysDictIndex() {
		// 字典管理
		return "/page/modular/system/dict/index.html";
	}

	@GetMapping("/page/system/dict/form.html")
	public String sysDictForm() {
		// 字典管理---编辑
		return "/page/modular/system/dict/form.html";
	}

	@GetMapping("/page/system/log_login/index.html")
	public String sysLoginLogndex() {
		// 访问日志
		return "/page/modular/system/log/log_login.html";
	}

	@GetMapping("/page/system/log_oper/index.html")
	public String sysOperLogndex() {
		// 操作日志
		return "/page/modular/system/log/log_oper.html";
	}

	@GetMapping("/page/system/log_error/index.html")
	public String sysErrorLogndex() {
		// 错误日志
		return "/page/modular/system/log/log_error.html";
	}

	@GetMapping("/page/system/monitor/index.html")
	public String sysMonitordex() {
		// 服务监控
		return "/page/modular/system/monitor/index.html";
	}

	@GetMapping("/page/system/onlineUser/index.html")
	public String sysOnlineUser() {
		// 服务监控
		return "/page/modular/system/onlineUser/index.html";
	}

	@GetMapping("/page/system/notice/index.html")
	public String sysNoticeIndex() {
		// 公告管理
		return "/page/modular/system/notice/index.html";
	}

	@GetMapping("/page/system/notice/form.html")
	public String sysNoticeForm() {
		// 公告管理---编辑
		return "/page/modular/system/notice/form.html";
	}

	@GetMapping("/page/system/fileInfo/index.html")
	public String sysFileInfo() {
		// 文件管理
		return "/page/modular/system/fileInfo/index.html";
	}

	@GetMapping("/page/system/redis/index.html")
	public String sysRedis() {
		// 文件管理
		return "/page/modular/system/redis/index.html";
	}

	@GetMapping("/page/timers/index.html")
	public String sysTimersIndex() {
		// 定时任务 --- 列表
		return "/page/modular/system/timer/index.html";
	}

	@GetMapping("/page/timers/form.html")
	public String sysTimersForm() {
		// 定时任务 --- 编辑
		return "/page/modular/system/timer/form.html";
	}

}
