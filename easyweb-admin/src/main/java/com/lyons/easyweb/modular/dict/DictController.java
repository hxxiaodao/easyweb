package com.lyons.easyweb.modular.dict;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.exception.BaseResponseCode;
import com.lyons.easyweb.exception.BusinessException;
import com.lyons.easyweb.modular.dict.pojo.Dict;
import com.lyons.easyweb.modular.dict.service.DictService;

/**
 * <p>
 * 字典 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/dict")
public class DictController {

	@Autowired
	private DictService dictService;
	
	
	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "dictCode", required = false) String dictCode, 
			@RequestParam(value = "dictName", required = false) String dictName, 
			@RequestParam(value = "page", defaultValue = "1") Integer current,
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<Dict> page = new Page<Dict>(current, limit);

		QueryWrapper<Dict> queryWrDicter = new QueryWrapper<Dict>();
		
		if (!ObjectUtils.isEmpty(dictCode)) {
			queryWrDicter.lambda().like(Dict::getDictCode, dictCode).or().like(Dict::getId, dictCode);
		}
		if (!ObjectUtils.isEmpty(dictName)) {
			queryWrDicter.lambda().like(Dict::getDictName, dictName);
		}
		
		queryWrDicter.lambda().orderByDesc(Dict::getSort).orderByDesc(Dict::getCreateTime);

		IPage<Dict> ipage = dictService.page(page, queryWrDicter);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid Dict dict) {

		dictService.insertOrUpdate(dict);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(dictService.getById(id));
	}

	// 修改状态
	@RequestMapping("/status")
	public LayuiPageInfo status(String id, boolean status) {

		Dict bean = dictService.getById(id);
		if (ObjectUtils.isEmpty(bean)) {
			throw new BusinessException(BaseResponseCode.DATA_ID_EMPTY);
		}
		if (status) {
			bean.setStatus(1);
		} else {
			bean.setStatus(0);
		}
		dictService.updateById(bean);
		return LayuiPageInfo.ok("状态修改成功 !");
	}
	
	// 查询所有（指定列）
	@RequestMapping("/listAll")
	public LayuiPageInfo listAll() {

		List<Dict> list = dictService.list(new QueryWrapper<Dict>().lambda().select(Dict::getId, Dict::getDictName));
		return LayuiPageInfo.ok(list);
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		return dictService.deleteById(id);
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		return dictService.batchDelete(ids);
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		return dictService.deleteAll();
	}

	
}
