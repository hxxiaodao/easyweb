package com.lyons.easyweb.modular.app;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.exception.BaseResponseCode;
import com.lyons.easyweb.exception.BusinessException;
import com.lyons.easyweb.modular.app.pojo.App;
import com.lyons.easyweb.modular.app.service.AppService;

/**
 * <p>
 * 系统应用表 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/app")
public class AppController {

	@Autowired
	private AppService appService;

	// 查询所有
	@GetMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "code", required = false) String code, 
			@RequestParam(value = "page", defaultValue = "1") Integer current, 
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<App> page = new Page<App>(current, limit);

		QueryWrapper<App> queryWrapper = new QueryWrapper<App>();
		if (!ObjectUtils.isEmpty(name)) {
			queryWrapper.lambda().like(App::getName, name).or().like(App::getId, name);
		}
		if (!ObjectUtils.isEmpty(code)) {
			queryWrapper.lambda().like(App::getCode, code);
		}
		queryWrapper.lambda().orderByDesc(App::getSort).orderByDesc(App::getCreateTime);

		IPage<App> ipage = appService.page(page, queryWrapper);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	// 添加或者修改
	@PostMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid App App) {

		appService.saveOrUpdate(App);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		
		return LayuiPageInfo.ok(appService.getById(id));
	}

	// 修改状态
	@RequestMapping("/status")
	public LayuiPageInfo status(String id, boolean status) {

		App bean = appService.getById(id);
		if (ObjectUtils.isEmpty(bean)) {
			throw new BusinessException(BaseResponseCode.DATA_ID_EMPTY);
		}
		if (status) {
			bean.setStatus(1);
		} else {
			bean.setStatus(0);
		}
		appService.updateById(bean);
		return LayuiPageInfo.ok("状态修改成功 !");
	}

	// 查询所有（指定列）
	@RequestMapping("/listAll")
	public LayuiPageInfo listAll() {

		List<App> list = appService.list(new QueryWrapper<App>().lambda().select(App::getId, App::getName).orderByDesc(App::getSort).orderByDesc(App::getCreateTime));
		return LayuiPageInfo.ok(list);
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		appService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		appService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		appService.remove(new QueryWrapper<App>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}

}
