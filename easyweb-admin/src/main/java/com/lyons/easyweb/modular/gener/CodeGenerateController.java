package com.lyons.easyweb.modular.gener;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 代码生成基础配置 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/gener/code-generate")
public class CodeGenerateController {

}
