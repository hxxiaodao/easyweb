package com.lyons.easyweb.modular.log;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.modular.log.pojo.OpLog;
import com.lyons.easyweb.modular.log.service.OpLogService;

/**
 * <p>
 * 系统操作日志表 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/log/oper")
public class OpLogController {
	
	@Autowired
	private OpLogService oplogService;

	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "opType", required = false) String opType, 
			@RequestParam(value = "success", required = false) String success, 
			@RequestParam(value = "location", required = false) String location, 
			@RequestParam(value = "page", defaultValue = "1") Integer current,
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {
	
		IPage<OpLog> page = new Page<OpLog>(current, limit);
	
		QueryWrapper<OpLog> queryWrOpLoger = new QueryWrapper<OpLog>();
		
		if (!ObjectUtils.isEmpty(name)) {
			queryWrOpLoger.lambda().like(OpLog::getName, name);
		}
		if (!ObjectUtils.isEmpty(opType)) {
			queryWrOpLoger.lambda().like(OpLog::getOpType, opType);
		}
		if (!ObjectUtils.isEmpty(success)) {
			queryWrOpLoger.lambda().like(OpLog::getSuccess, success);
		}
		if (!ObjectUtils.isEmpty(location)) {
			queryWrOpLoger.lambda().like(OpLog::getLocation, location);
		}
		
		queryWrOpLoger.lambda().orderByDesc(OpLog::getOpTime);
	
		IPage<OpLog> ipage = oplogService.page(page, queryWrOpLoger);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}
	
	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid OpLog OpLog) {
	
		oplogService.saveOrUpdate(OpLog);
		return LayuiPageInfo.ok("数据保存成功 !");
	}
	
	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(oplogService.getById(id));
	}
	
	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {
	
		oplogService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}
	
	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {
	
		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());
	
		oplogService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}
	
	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {
	
		oplogService.remove(new QueryWrapper<OpLog>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}

}
