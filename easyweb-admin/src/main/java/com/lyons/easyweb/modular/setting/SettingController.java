package com.lyons.easyweb.modular.setting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.core.util.RedisOperator;
import com.lyons.easyweb.core.util.ResponseData;
import com.lyons.easyweb.modular.setting.pojo.Setting;
import com.lyons.easyweb.modular.setting.service.SettingService;

/**
 * <p>
 * 系统设置 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-05
 */
@RestController
@RequestMapping("/sys/setting")
public class SettingController {

	@Autowired
	private RedisOperator redis;

	@Autowired
	private SettingService settingServvice;

	@RequestMapping("/querySetting")
	public LayuiPageInfo querySetting() {

		Setting setting = settingServvice.getOne(new QueryWrapper<Setting>().last("LIMIT 1"));

		return LayuiPageInfo.ok(setting);
	}

	@RequestMapping("/saveSetting")
	public LayuiPageInfo saveSetting(@RequestBody Setting setting) {

		settingServvice.saveOrUpdate(setting);

		redis.delete("system-setting");

		return LayuiPageInfo.ok("平台信息保存成功 ！");
	}

	@RequestMapping("/querySettigInfo")
	public ResponseData querySettigInfo() {

		Setting setting = settingServvice.querySettingInfo();

		return ResponseData.ok(setting);
	}


}
