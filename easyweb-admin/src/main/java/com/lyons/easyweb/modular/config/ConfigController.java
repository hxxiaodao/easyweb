package com.lyons.easyweb.modular.config;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.exception.BaseResponseCode;
import com.lyons.easyweb.exception.BusinessException;
import com.lyons.easyweb.modular.config.pojo.Config;
import com.lyons.easyweb.modular.config.service.ConfigService;

/**
 * <p>
 * 参数配置 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/config")
public class ConfigController {

	@Autowired
	private ConfigService configService;

	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "configName", required = false) String configName, 
			@RequestParam(value = "configCode", required = false) String configCode, 
			@RequestParam(value = "groupCode", required = false) String groupCode,
			@RequestParam(value = "page", defaultValue = "1") Integer current, 
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<Config> page = new Page<Config>(current, limit);

		QueryWrapper<Config> queryWrConfiger = new QueryWrapper<Config>();
		if (!ObjectUtils.isEmpty(configName)) {
			queryWrConfiger.lambda().like(Config::getConfigName, configName).or().like(Config::getId, configName);
		}
		if (!ObjectUtils.isEmpty(configCode)) {
			queryWrConfiger.lambda().like(Config::getConfigCode, configCode);
		}
		if (!ObjectUtils.isEmpty(groupCode)) {
			queryWrConfiger.lambda().eq(Config::getGroupCode, groupCode);
		}

		queryWrConfiger.lambda().orderByDesc(Config::getSort).orderByDesc(Config::getCreateTime);

		IPage<Config> ipage = configService.page(page, queryWrConfiger);
		
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid Config config) {

		return configService.insertOrUpdate(config);
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(configService.getById(id));
	}

	// 修改状态
	@RequestMapping("/status")
	public LayuiPageInfo status(String id, boolean status) {

		Config bean = configService.getById(id);
		if (ObjectUtils.isEmpty(bean)) {
			throw new BusinessException(BaseResponseCode.DATA_ID_EMPTY);
		}
		if (status) {
			bean.setStatus(1);
		} else {
			bean.setStatus(0);
		}
		configService.updateById(bean);
		return LayuiPageInfo.ok("状态修改成功 !");
	}

	// 查询所有（指定列）
	@RequestMapping("/listAll")
	public LayuiPageInfo listAll() {

		List<Config> list = configService.list(new QueryWrapper<Config>().lambda().select(Config::getId, Config::getConfigName));
		return LayuiPageInfo.ok(list);
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		return configService.delete(id);
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		return configService.batchDelete(ids);
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		return configService.deleteAll();
	}

}
