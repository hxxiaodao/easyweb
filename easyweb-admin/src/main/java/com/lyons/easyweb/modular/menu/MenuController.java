package com.lyons.easyweb.modular.menu;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lyons.easyweb.core.util.Constant;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.exception.BaseResponseCode;
import com.lyons.easyweb.exception.BusinessException;
import com.lyons.easyweb.modular.menu.pojo.Menu;
import com.lyons.easyweb.modular.menu.service.MenuService;
import com.lyons.easyweb.modular.menu.vo.resp.MenuVoResp;
import com.lyons.easyweb.modular.user.vo.resp.LoginRespVo;

/**
 * <p>
 * 菜单管理 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/menu")
public class MenuController {

	@Autowired
	private MenuService menuService;

	// 查询所有(layui mini 菜单接口使用)
	@RequestMapping("/list")
	public Map<String, Object> list(HttpServletRequest request) {

		// 获得用户ID，根据用户ID查询对应的菜单数据
		LoginRespVo user = (LoginRespVo) request.getSession().getAttribute(Constant.LOGIN_USER);

		return menuService.listMenu(user.getUserId());
	}

	/**
	 * ztree 查询菜单数据（添加菜单或者角色授权菜单使用）.roleId 为空查询所有的菜单，roleId不为空，者角色选中的菜单默认为选中状态
	 * 
	 * @param roleId 角色ID
	 * @return
	 */
	@RequestMapping("/queryAllModules")
	public LayuiPageInfo queryAllModules(@RequestParam(value = "roleId", required = false) String roleId) {

		return LayuiPageInfo.ok(menuService.queryAllModules(roleId));
	}

	// xm-select (角色和添加菜单使用)
	@RequestMapping("/queryMenuList")
	public LayuiPageInfo queryMenuList(
			@RequestParam(value = "roleId", required = false) String roleId,
			@RequestParam(value = "id", required = false) String id) {

		List<MenuVoResp> list = menuService.queryMenuList(roleId,id);
		return LayuiPageInfo.ok(list);
	}

	// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
	@RequestMapping("/listAllPage")
	public LayuiPageInfo listAllPage() {

		List<Menu> list = menuService.list();
		return LayuiPageInfo.ok(list);
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid Menu Menu) {

		menuService.saveOrUpdate(Menu);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(menuService.getById(id));
	}

	// 修改状态
	@RequestMapping("/status")
	public LayuiPageInfo status(String id, boolean status) {

		Menu bean = menuService.getById(id);
		if (ObjectUtils.isEmpty(bean)) {
			throw new BusinessException(BaseResponseCode.DATA_ID_EMPTY);
		}
		if (status) {
			bean.setStatus(1);
		} else {
			bean.setStatus(0);
		}
		menuService.updateById(bean);
		return LayuiPageInfo.ok("状态修改成功 !");
	}

	// 查询所有（指定列）
	@RequestMapping("/listAll")
	public LayuiPageInfo listAll() {

		List<Menu> list = menuService.list(new QueryWrapper<Menu>().lambda().select(Menu::getId, Menu::getPid, Menu::getName));
		return LayuiPageInfo.ok(list);
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		menuService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		menuService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		menuService.remove(new QueryWrapper<Menu>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}

}
