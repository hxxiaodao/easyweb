package com.lyons.easyweb.modular.notice;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.exception.BaseResponseCode;
import com.lyons.easyweb.exception.BusinessException;
import com.lyons.easyweb.modular.notice.pojo.Notice;
import com.lyons.easyweb.modular.notice.service.NoticeService;

/**
 * <p>
 * 通知表 前端控制器
 * </p>
 *
 * @author HengDuCms
 * @since 2021-06-28
 */
@RestController
@RequestMapping("/sys/notice")
public class NoticeController {
	
	
	@Autowired
	private NoticeService noticeService;
	
	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "title", required = false) String title, 
			@RequestParam(value = "readStatus", required = false) String readStatus, 
			@RequestParam(value = "page", defaultValue = "1") Integer current,
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<Notice> page = new Page<Notice>(current, limit);

		QueryWrapper<Notice> queryWrNoticeer = new QueryWrapper<Notice>();
		
		if (!ObjectUtils.isEmpty(title)) {
			queryWrNoticeer.lambda().like(Notice::getTitle, title).or().like(Notice::getId, title);
		}
		if (!ObjectUtils.isEmpty(readStatus)) {
			queryWrNoticeer.lambda().like(Notice::getReadStatus, readStatus);
		}
		
		queryWrNoticeer.lambda().orderByDesc(Notice::getCreateTime);

		IPage<Notice> ipage = noticeService.page(page, queryWrNoticeer);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid Notice Notice) {

		noticeService.saveOrUpdate(Notice);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(noticeService.getById(id));
	}

	// 修改状态
	@RequestMapping("/status")
	public LayuiPageInfo status(String id, boolean status) {

		Notice bean = noticeService.getById(id);
		if (ObjectUtils.isEmpty(bean)) {
			throw new BusinessException(BaseResponseCode.DATA_ID_EMPTY);
		}
		if (status) {
			bean.setReadStatus(1);
		} else {
			bean.setReadStatus(0);
		}
		noticeService.updateById(bean);
		return LayuiPageInfo.ok("状态修改成功 !");
	}

	// 查询所有（指定列）
	@RequestMapping("/listAll")
	public LayuiPageInfo listAll() {

		List<Notice> list = noticeService.list(new QueryWrapper<Notice>().lambda().select(Notice::getId, Notice::getTitle));
		return LayuiPageInfo.ok(list);
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		noticeService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		noticeService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		noticeService.remove(new QueryWrapper<Notice>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}
}
