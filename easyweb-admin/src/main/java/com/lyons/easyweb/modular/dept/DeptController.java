package com.lyons.easyweb.modular.dept;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.exception.BaseResponseCode;
import com.lyons.easyweb.exception.BusinessException;
import com.lyons.easyweb.modular.dept.pojo.Dept;
import com.lyons.easyweb.modular.dept.service.DeptService;

/**
 * <p>
 * 部门信息 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/dept")
public class DeptController {

	@Autowired
	private DeptService deptService;

	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "deptNo", required = false) String deptNo, 
			@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "page", defaultValue = "1") Integer current, 
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<Dept> page = new Page<Dept>(current, limit);

		QueryWrapper<Dept> queryWrDepter = new QueryWrapper<Dept>();

		if (!ObjectUtils.isEmpty(deptNo)) {
			queryWrDepter.lambda().like(Dept::getDeptNo, deptNo).or().like(Dept::getId, deptNo);
		}
		if (!ObjectUtils.isEmpty(name)) {
			queryWrDepter.lambda().like(Dept::getName, name);
		}

		queryWrDepter.lambda().orderByDesc(Dept::getSort).orderByDesc(Dept::getCreateTime);

		IPage<Dept> ipage = deptService.page(page, queryWrDepter);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid Dept Dept) {

		deptService.saveOrUpdate(Dept);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(deptService.getById(id));
	}

	// 修改状态
	@RequestMapping("/status")
	public LayuiPageInfo status(String id, boolean status) {

		Dept bean = deptService.getById(id);
		if (ObjectUtils.isEmpty(bean)) {
			throw new BusinessException(BaseResponseCode.DATA_ID_EMPTY);
		}
		if (status) {
			bean.setStatus(1);
		} else {
			bean.setStatus(0);
		}
		deptService.updateById(bean);
		return LayuiPageInfo.ok("状态修改成功 !");
	}

	// 查询所有（指定列）
	@RequestMapping("/listAll")
	public LayuiPageInfo listAll() {

		List<Dept> list = deptService.list(new QueryWrapper<Dept>().lambda().select(Dept::getId, Dept::getName));
		return LayuiPageInfo.ok(list);
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		deptService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		deptService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		deptService.remove(new QueryWrapper<Dept>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}

}
