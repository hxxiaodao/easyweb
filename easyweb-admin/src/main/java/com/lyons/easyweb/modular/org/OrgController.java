package com.lyons.easyweb.modular.org;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.exception.BaseResponseCode;
import com.lyons.easyweb.exception.BusinessException;
import com.lyons.easyweb.modular.org.pojo.Org;
import com.lyons.easyweb.modular.org.service.OrgService;

/**
 * <p>
 * 系统组织机构表 前端控制器
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys/org")
public class OrgController {
	
	
	@Autowired
	private OrgService orgService;
	
	// 查询所有
	@RequestMapping("/list")
	public LayuiPageInfo list(
			@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "code", required = false) String code, 
			@RequestParam(value = "page", defaultValue = "1") Integer current,
			@RequestParam(value = "limit", defaultValue = "20") Integer limit) {

		IPage<Org> page = new Page<Org>(current, limit);

		QueryWrapper<Org> queryWrOrger = new QueryWrapper<Org>();
		
		if (!ObjectUtils.isEmpty(name)) {
			queryWrOrger.lambda().like(Org::getName, name).or().like(Org::getId, name);
		}
		if (!ObjectUtils.isEmpty(code)) {
			queryWrOrger.lambda().like(Org::getCode, code);
		}
		
		queryWrOrger.lambda().orderByDesc(Org::getSort).orderByDesc(Org::getCreateTime);

		IPage<Org> ipage = orgService.page(page, queryWrOrger);
		return LayuiPageInfo.ok(ipage.getTotal(), ipage.getRecords());
	}

	// 添加或者修改
	@RequestMapping("/saveOrUpdate")
	public LayuiPageInfo saveOrUpdate(@RequestBody @Valid Org Org) {

		orgService.saveOrUpdate(Org);
		return LayuiPageInfo.ok("数据保存成功 !");
	}

	// 查询详情
	@RequestMapping("/findById")
	public LayuiPageInfo findById(String id) {
		return LayuiPageInfo.ok(orgService.getById(id));
	}

	// 修改状态
	@RequestMapping("/status")
	public LayuiPageInfo status(String id, boolean status) {

		Org bean = orgService.getById(id);
		if (ObjectUtils.isEmpty(bean)) {
			throw new BusinessException(BaseResponseCode.DATA_ID_EMPTY);
		}
		if (status) {
			bean.setStatus(1);
		} else {
			bean.setStatus(0);
		}
		orgService.updateById(bean);
		return LayuiPageInfo.ok("状态修改成功 !");
	}

	// 查询所有（指定列）
	@RequestMapping("/listAll")
	public LayuiPageInfo listAll() {

		List<Org> list = orgService.list(new QueryWrapper<Org>().lambda().select(Org::getId, Org::getName));
		return LayuiPageInfo.ok(list);
	}

	// 按照ID删除
	@RequestMapping("/delete")
	public LayuiPageInfo delete(String id) {

		orgService.removeById(id);
		return LayuiPageInfo.ok("数据删除成功 !");
	}

	// 批量删除
	@RequestMapping("/batchDelete")
	public LayuiPageInfo batchDelete(@RequestBody String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		orgService.removeByIds(idList);
		return LayuiPageInfo.ok("成功删除了 [ " + idList.size() + " ] 条数据 ！");
	}

	// 删除所有
	@RequestMapping("/deleteAll")
	public LayuiPageInfo deleteAll() {

		orgService.remove(new QueryWrapper<Org>());
		return LayuiPageInfo.ok("成功删除表中所有数 !");
	}
	
}
