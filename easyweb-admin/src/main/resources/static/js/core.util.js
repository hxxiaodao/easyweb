
function sendAjax(url, params, ft, method, headers, noAuthorityFt, async, contentType) {
	var roleSaveLoading = top.layer.msg('数据提交中，请稍候...', { icon: 16, time: false, shade: 0.8 });
	layui.jquery.ajax({
		url: url,
		cache: false,
		async: async == undefined ? true : async,
		data: params,
		type: method == undefined ? "POST" : method,
		contentType: contentType == undefined ? 'application/json;charset=UTF-8' : contentType,
		dataType: "json",
	/*	beforeSend: function(request) {
			if (headers == undefined) {
			
			} else if (headers) {
				request.setRequestHeader("authorization", CoreUtil.getData("access_token"));
				request.setRequestHeader("refreshToken", CoreUtil.getData("refresh_token"));
			} else {
				request.setRequestHeader("authorization", CoreUtil.getData("access_token"));
			}

		},*/
		success: function(res) {
			top.layer.close(roleSaveLoading);
			if (typeof ft == "function") {
				if (res.code == 4010001) { //凭证过期重新登录
					layer.msg("凭证过期请重新登录")
					top.window.location.href = "/index/login"
				} else if (res.code == 4010002) {//根据后端提示刷新token
					/*记录要重复刷新的参数*/
					var reUrl = url;
					var reParams = params;
					var reFt = ft;
					var reMethod = method;
					var reHeaders = headers;
					var reContentType = contentType;
					var reAsync = async;
					var reNoAuthorityFt = noAuthorityFt;
					/*刷新token  然后存入缓存*/
					sendAjax("/api/user/token", null, function(res) {
						if (res.code == 0) {
							setData("access_token", res.data);
							setTimeout(function() {
								/*刷新成功后继续重复请求*/
								CoreUtil.sendAjax(reUrl, reParams, reFt, reMethod, reHeaders, reNoAuthorityFt, reContentType, reAsync);
							}, 1000);
						} else {
							layer.msg("凭证过期请重新登录",{icon:0,time:2000});
							top.window.location.href = "/page/login.html";
						}
					}, "GET", true)
				} else if (res.code == 0) {
					if (ft != null && ft != undefined) {
						ft(res);
					}
				}  else if (res.code == 1) {
					if (ft != null && ft != undefined) {
						ft(res);
					}
				} else if (res.code == 4030001) {
					if (ft != null && ft != undefined) {
						noAuthorityFt(res);
					}
				} else {
					layer.msg(res.msg,{icon:0,time:3000})
				}
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			top.layer.close(roleSaveLoading);
			if (XMLHttpRequest.status == 404) {
				top.window.location.href = "/index/404";
			} else {
				layer.alert("服务器好像除了点问题！请稍后试试 !",{icon:2,time:3000});
			}
		}
	});
}

function setData(key, value) {
	layui.sessionData('LocalData', {
		key: key,
		value: value
	});
}

function getData(key) {
	var localData = layui.sessionData('LocalData');
	return localData[key];
}

function getNowFormatDate() {
	var date = new Date();
	var seperator1 = "-";
	var seperator2 = ":";
	var month = date.getMonth() + 1;
	var strDate = date.getDate();
	if (month >= 1 && month <= 9) {
		month = "0" + month;
	}
	if (strDate >= 0 && strDate <= 9) {
		strDate = "0" + strDate;
	}
	var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate + " " + date.getHours() + seperator2 + date.getMinutes() + seperator2 + date.getSeconds();
	return currentdate;
}

/* 获取URL指定参数的值 */
function request(paras) {
	var url = location.href;
	var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
	var paraObj = {}
	for (i = 0; j = paraString[i]; i++) {
		paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
	}
	var returnValue = paraObj[paras.toLowerCase()];
	if (typeof (returnValue) == "undefined") {
		return "";
	} else {
		return returnValue;
	}
}
