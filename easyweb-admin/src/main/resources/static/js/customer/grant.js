
// 定义树形结构对象
var zTreeObj;

/*
 * 加载树形结构数据
 */
function loadModuleData() {
	// 配置信息对象 zTree 参数配置
	var setting = {
		check : {
			enable : true
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		callback : {
			onCheck : zTreeOnCheck
		}
	}
	// 数据 === 传递角色ID，查询当前角色已经授权的资源
	$.ajax({
		type : 'GET',
		url : '/sys/menu/queryAllModules',
		data: {
			roleId: $("#id").val()
		},
		dataType : 'json',
		success : function(data) {
			 console.log(data);
			zTreeObj = $.fn.zTree.init($("#test"), setting, data);
		}
	});
}
/*
 * 每次点击 checkbox 或 radio 后， 弹出该节点的 tId、name 以及当前勾选状态的信息
 */
function zTreeOnCheck(event, treeId, treeNode) {

	console.log(treeNode.tId + ", " + treeNode.name + "," + treeNode.checked);

	// 获取所有被勾选的节点集合，如果checked = true 表示获取勾选的节点，false 表示获取未勾选的节点
	var nodes = zTreeObj.getCheckedNodes(true);
	console.log(nodes);
	// 获取所有的资源 id 值
	if (nodes.length > 0) { // 有选择节点
		var mIds = "mIds=";
		for (var i = 0; i < nodes.length; i++) {
			if (i < nodes.length - 1) {
				mIds += nodes[i].id + "&mIds=";
			} else {
				mIds += nodes[i].id;
			}
		}
		console.log(mIds);
	}

	var roleId = $("#hiddenValue").val();
	console.log("roleId = " + roleId);
	// Ajax 将数据发送到后台
	$.ajax({
		type : 'POST',
		url : '/sysRole/ztreeGrant',
		data : mIds + "&roleId=" + roleId,
		dataType : 'json',
		success : function(resp) {
			console.log(resp);
			
			layui.use('layer', function() {
				var layer = layui.layer;
				layer.msg(resp.msg,{icon:1,time:500});
			});
		}
	});

};