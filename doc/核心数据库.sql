DROP table if EXISTS sys_user;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `real_name` varchar(100) DEFAULT NULL COMMENT '姓名',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `account` varchar(50) NOT NULL COMMENT '账号',
  `password` varchar(64) NOT NULL COMMENT '密码',
  `salt` varchar(64) DEFAULT NULL COMMENT '盐',
  `avatar` bigint(20) DEFAULT NULL COMMENT '头像，存的为文件id',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `sex` tinyint(4) DEFAULT NULL COMMENT '性别：0-男，1-女',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) DEFAULT NULL COMMENT '手机',
  `tel` varchar(50) DEFAULT NULL COMMENT '电话',
  `super_admin_flag` tinyint(4) DEFAULT '0' COMMENT '是否是超级管理员：1-是，0-否',
  `last_login_ip` varchar(100) DEFAULT NULL COMMENT '最后登陆IP',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `del_flag` tinyint(4) DEFAULT '1' COMMENT '删除标记：0-已删除，1-未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态[0-禁用;1-激活;]',
  PRIMARY KEY (`id`)
)COMMENT='系统用户';
-- superAdmin/*963.
INSERT INTO `sys_user` VALUES ('1409840279506259968', null, null, 'superAdmin', 'ffd896d7af437be2fd8b89aaa7f4a7db', null, null, null, '0', null, null, null, '1', null, null, '1', '2021-06-29 19:45:05', '2021-06-29 19:45:05', null, null, '1');


DROP table if EXISTS sys_role;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(300) DEFAULT NULL comment'描述',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` tinyint(4) DEFAULT '1' COMMENT '是否删除(1未删除；0已删除)',
   `status` tinyint(4) DEFAULT '1' COMMENT '状态(1:正常0:弃用)',
  PRIMARY KEY (`id`)
) COMMENT='角色表';


DROP table if EXISTS sys_user_role;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色id',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) COMMENT='用户与角色关系';


DROP table if EXISTS sys_menu;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL,
  `pid` bigint(20) DEFAULT '0' COMMENT '父菜单ID，一级菜单为0',
  `pids` text NOT NULL COMMENT '父ids',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `router` varchar(255) DEFAULT NULL COMMENT '路由地址',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型  0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `application` bigint(20) NOT NULL COMMENT '应用分类[应用编码]',
  `weight` tinyint(4) DEFAULT NULL COMMENT '权重[字典 1系统权重 2业务权重]',
  `remark` varchar(500) comment'描述',
  `sort` int(11) DEFAULT NULL COMMENT '排序[值越大越靠前]',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态[0-禁用；1- 激活]',
  PRIMARY KEY (`id`)
)COMMENT='菜单管理';


DROP table if EXISTS sys_role_menu;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
)COMMENT='角色与菜单对应关系';


DROP table if EXISTS sys_notice;
CREATE TABLE `sys_notice` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `suId` bigint(20) DEFAULT NULL COMMENT '通知发送者',
  `reId` bigint(20) DEFAULT NULL COMMENT '通知接收者',
  `read_status` tinyint(4) DEFAULT NULL COMMENT '阅读状态[0-已阅读;1-未阅读]',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
)COMMENT='通知表';

DROP table if EXISTS sys_dict_type;
CREATE TABLE `sys_dict_type` (
  `id` bigint(20) NOT NULL COMMENT '字典类型id',
  `dict_type_class` tinyint(4) DEFAULT NULL COMMENT '字典类型:1-业务类型，2-系统类型',
  `dict_type_bus_code` varchar(255) DEFAULT NULL COMMENT '字典类型业务编码',
  `dict_type_code` varchar(255) DEFAULT NULL COMMENT '字典类型编码',
  `dict_type_name` varchar(255) DEFAULT NULL COMMENT '字典类型名称',
  `dict_type_name_pinyin` varchar(255) DEFAULT NULL COMMENT '字典类型名称首字母拼音',
  `dict_type_desc` varchar(255) DEFAULT NULL COMMENT '字典类型描述',
  `sort` int DEFAULT 100 COMMENT '排序，大靠前',
  `del_flag` char(1) NOT NULL DEFAULT 'N' COMMENT '是否删除：Y-被删除，N-未删除',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户id',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_user` bigint(20) DEFAULT NULL COMMENT '修改用户id',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态[0-禁用;1-激活;]',
  PRIMARY KEY (`id`)
) COMMENT='字典类型';


DROP table if EXISTS sys_dict;
CREATE TABLE `sys_dict` (
  `id` bigint(20) NOT NULL COMMENT '字典id',
  `pid` bigint(20) DEFAULT '0' COMMENT '上级字典的id(如果没有上级字典id，则为0)',
  `pids` varchar(1000) NOT NULL COMMENT '父id集合',
  `dict_code` varchar(255) NOT NULL COMMENT '字典编码',
  `dict_name` varchar(255) NOT NULL COMMENT '字典名称',
  `dict_name_pinyin` varchar(255) DEFAULT NULL COMMENT '字典名称首字母',
  `dict_type_code` varchar(255) NOT NULL COMMENT '字典类型的编码',
  `status_flag` tinyint(4) NOT NULL COMMENT '状态：(1-启用,2-禁用)',
  `sort` int DEFAULT 100 COMMENT '排序，大靠前',
  `del_flag` char(1) NOT NULL DEFAULT 'N' COMMENT '是否删除，Y-被删除，N-未删除',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户id',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_user` bigint(20) DEFAULT NULL COMMENT '修改用户id',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态[0-禁用;1-激活;]',
  PRIMARY KEY (`id`)
)COMMENT='字典';


DROP table if EXISTS sys_config;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `group_code` varchar(255) DEFAULT NULL COMMENT '常量所属分类的编码',
  `config_name` varchar(255) NOT NULL COMMENT '名称',
  `config_code` varchar(255) NOT NULL COMMENT '编码',
  `config_value` text NOT NULL COMMENT '属性值',
  `sort` int DEFAULT 100 COMMENT '排序，大靠前',
  `sys_flag` char(1) DEFAULT 'Y' COMMENT '是否是系统参数：Y-是，N-否',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) NOT NULL DEFAULT 'N' COMMENT '是否删除，Y-被删除，N-未删除',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户id',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_user` bigint(20) DEFAULT NULL COMMENT '修改用户id',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态[0-禁用;1-激活;]',
  PRIMARY KEY (`id`)
) COMMENT='参数配置';


DROP table if EXISTS sys_file_info;
CREATE TABLE `sys_file_info` (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `file_location` tinyint(4) NOT NULL COMMENT '文件存储位置[1:阿里云，2:腾讯云，3:minio，4:本地]',
  `file_bucket` varchar(1000) DEFAULT NULL COMMENT '文件仓库',
  `file_origin_name` varchar(100) NOT NULL COMMENT '文件名称[上传时候的文件名]',
  `file_suffix` varchar(50) DEFAULT NULL COMMENT '文件后缀',
  `file_size_kb` bigint(20) DEFAULT NULL COMMENT '文件大小kb',
  `file_size_info` varchar(100) DEFAULT NULL COMMENT '文件大小信息，计算后的',
  `file_object_name` varchar(100) NOT NULL COMMENT '存储到bucket的名称[文件唯一标识id]',
  `file_path` varchar(1000) DEFAULT NULL COMMENT '存储路径',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` bigint(20) DEFAULT NULL COMMENT '修改用户',
  PRIMARY KEY (`id`)
) COMMENT='文件信息表';


DROP table if EXISTS sys_dept;
CREATE TABLE `sys_dept` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `dept_no` bigint(18) DEFAULT NULL COMMENT '部门编号',
  `name` varchar(300) DEFAULT NULL COMMENT '部门名称',
   sort int default 100 comment'排序,大靠前',
  `pid` varchar(64) NOT NULL COMMENT '父级id',
  `relation_code` varchar(3000) DEFAULT NULL COMMENT '为了维护更深层级关系(规则：父级关系编码+自己的编码)',
  `dept_manager_id` varchar(64) DEFAULT NULL COMMENT '部门经理user_id',
  `manager_name` varchar(255) DEFAULT NULL COMMENT '部门经理名称',
  `phone` varchar(20) DEFAULT NULL COMMENT '部门经理联系电话',
 `del_flag` char(1) NOT NULL DEFAULT 'N' COMMENT '是否删除，Y-被删除，N-未删除',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户id',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_user` bigint(20) DEFAULT NULL COMMENT '修改用户id',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态[0-禁用;1-激活;]',
  PRIMARY KEY (`id`)
)COMMENT='部门信息';

DROP table if EXISTS sys_op_log;
CREATE TABLE `sys_op_log` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `op_type` tinyint(4) DEFAULT NULL COMMENT '操作类型',
  `success` char(1) DEFAULT NULL COMMENT '是否执行成功[Y-是，N-否]',
  `message` text COMMENT '具体消息',
  `ip` varchar(255) DEFAULT NULL COMMENT 'ip',
  `location` varchar(255) DEFAULT NULL COMMENT '地址',
  `browser` varchar(255) DEFAULT NULL COMMENT '浏览器',
  `os` varchar(255) DEFAULT NULL COMMENT '操作系统',
  `url` varchar(500) DEFAULT NULL COMMENT '请求地址',
  `class_name` varchar(500) DEFAULT NULL COMMENT '类名称',
  `method_name` varchar(500) DEFAULT NULL COMMENT '方法名称',
  `req_method` varchar(255) DEFAULT NULL COMMENT '请求方式[GET POST PUT DELETE)',
  `param` text COMMENT '请求参数',
  `result` longtext COMMENT '返回结果',
  `op_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  `account` varchar(50) DEFAULT NULL COMMENT '操作账号',
  PRIMARY KEY (`id`)
) COMMENT='系统操作日志表';


DROP table if EXISTS sys_error_log;
CREATE TABLE `sys_error_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device` tinyint(4) DEFAULT NULL COMMENT '设备信息[1-WEB;2-App]',
  `class_name` varchar(100) DEFAULT NULL COMMENT '错误类名',
  `error_msg` varchar(2000) DEFAULT NULL COMMENT '错误描述',
  `exception` text COMMENT '错误详细信息',
  `url` varchar(255) DEFAULT NULL COMMENT '请求地址',
  `method` varchar(50) DEFAULT NULL COMMENT '请求方法',
  `param` varchar(255) DEFAULT NULL COMMENT '参数',
  `ip` varchar(255) DEFAULT NULL COMMENT '用户IP',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '日志产生时间',
  PRIMARY KEY (`id`)
) COMMENT='错误信息';


DROP table if EXISTS sys_login_log;
CREATE TABLE `sys_login_log` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `success` char(1) DEFAULT NULL COMMENT '是否执行成功[Y-是，N-否]',
  `message` text COMMENT '具体消息',
  `ip` varchar(255) DEFAULT NULL COMMENT 'ip',
  `location` varchar(255) DEFAULT NULL COMMENT '地址',
  `browser` varchar(255) DEFAULT NULL COMMENT '浏览器',
  `os` varchar(255) DEFAULT NULL COMMENT '操作系统',
  `vis_type` tinyint(4) NOT NULL COMMENT '操作类型[ 1登入 2登出]',
  `vis_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '访问时间',
  `account` varchar(50) DEFAULT NULL COMMENT '访问账号',
  PRIMARY KEY (`id`)
) COMMENT='系统访问日志表';


DROP table if EXISTS sys_app;
CREATE TABLE `sys_app` (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `name` varchar(100) NOT NULL COMMENT '应用名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  sort int default 100 comment'排序,大靠前',
  `del_flag` tinyint(4) DEFAULT '1' COMMENT '删除标记：0-已删除，1-未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态[0-禁用;1-激活;]',
  PRIMARY KEY (`id`)
)COMMENT='系统应用表';


DROP table if EXISTS sys_org;
CREATE TABLE `sys_org` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `pid` bigint(20) NOT NULL COMMENT '父id',
  `pids` text NOT NULL COMMENT '父ids',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `sort` int(11) NOT NULL COMMENT '排序',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态（字典 0正常 1停用 2删除）',
`create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '域名添加时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='系统组织机构表';

DROP TABLE if EXISTS sys_pos;
CREATE TABLE `sys_pos` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `code` varchar(50) NOT NULL COMMENT '编码',
  `sort` int(11) NOT NULL COMMENT '排序',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态（字典 0正常 1停用 2删除）',
`create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '域名添加时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `CODE_UNI` (`code`) USING BTREE
)COMMENT='系统职位表';

DROP table if EXISTS sys_code_generate;
CREATE TABLE `sys_code_generate` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `author_name` varchar(255) NOT NULL COMMENT '作者姓名',
  `class_name` varchar(255) NOT NULL COMMENT '类名',
  `table_prefix` varchar(255) NOT NULL COMMENT '是否移除表前缀',
  `generate_type` varchar(255) NOT NULL COMMENT '生成位置类型',
  `table_name` varchar(255) NOT NULL COMMENT '数据库表名',
  `package_name` varchar(255) DEFAULT NULL COMMENT '包名称',
  `bus_name` varchar(255) DEFAULT NULL COMMENT '业务名',
  `table_comment` varchar(255) DEFAULT NULL COMMENT '功能名',
 `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '域名添加时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
)COMMENT='代码生成基础配置';


DROP table if EXISTS sys_code_generate_config;
CREATE TABLE `sys_code_generate_config` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `code_gen_id` bigint(20) DEFAULT NULL COMMENT '代码生成主表ID',
  `column_name` varchar(255) DEFAULT NULL COMMENT '数据库字段名',
  `java_name` varchar(255) DEFAULT NULL COMMENT 'java类字段名',
  `data_type` varchar(255) DEFAULT NULL COMMENT '物理类型',
  `column_comment` varchar(255) DEFAULT NULL COMMENT '字段描述',
  `java_type` varchar(255) DEFAULT NULL COMMENT 'java类型',
  `effect_type` varchar(255) DEFAULT NULL COMMENT '作用类型（字典）',
  `dict_type_code` varchar(255) DEFAULT NULL COMMENT '字典code',
  `whether_table` varchar(255) DEFAULT NULL COMMENT '列表展示',
  `whether_add_update` varchar(255) DEFAULT NULL COMMENT '增改',
  `whether_retract` varchar(255) DEFAULT NULL COMMENT '列表是否缩进（字典）',
  `whether_required` varchar(255) DEFAULT NULL COMMENT '是否必填（字典）',
  `query_whether` varchar(255) DEFAULT NULL COMMENT '是否是查询条件',
  `query_type` varchar(255) DEFAULT NULL COMMENT '查询方式',
  `column_key` varchar(255) DEFAULT NULL COMMENT '主键',
  `column_key_name` varchar(255) DEFAULT NULL COMMENT '主外键名称',
  `whether_common` varchar(255) DEFAULT NULL COMMENT '是否是通用字段',
`create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '域名添加时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_user` bigint(20) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
)COMMENT='代码生成详细配置';
