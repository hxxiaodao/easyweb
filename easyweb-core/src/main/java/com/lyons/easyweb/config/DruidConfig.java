package com.lyons.easyweb.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.ObjectUtils;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyons.easyweb.modular.config.pojo.Config;
import com.lyons.easyweb.modular.config.service.ConfigService;

import cn.hutool.core.util.RandomUtil;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Configuration
public class DruidConfig {

	@Lazy
	@Autowired
	private ConfigService configService;

	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DruidDataSource getDataSource() {
		return new DruidDataSource();
	}

	@Bean
	public ServletRegistrationBean druidStatViewServlet() {

		// ServletRegistrationBean提供类的进行注册
		ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");

		// 添加初始化参数：initParams
		// 白名单：
		servletRegistrationBean.addInitParameter("allow", "127.0.0.1");

		// IP黑名单（同时存在时，deny优先于allow）
		// 如果满足deny，就提示：sorry，you are not permitted to view this page
		servletRegistrationBean.addInitParameter("deny", "192.168.1.73");

		Config druidUserName = configService.getOne(new LambdaQueryWrapper<Config>().eq(Config::getConfigCode, "DRUID_ACCOUNT"));

		if (ObjectUtils.isEmpty(druidUserName)) {
			System.out.println("字典未配置 druid 账号,设置随机账号：admin，如不想有此提示信息，可以在系统配置中添加 [DRUID_ACCOUNT] 值");
			servletRegistrationBean.addInitParameter("loginUsername", "admin");
		} else {
			servletRegistrationBean.addInitParameter("loginUsername", druidUserName.getConfigValue());
		}

		Config druidPassword = configService.getOne(new LambdaQueryWrapper<Config>().eq(Config::getConfigCode, "DRUID_PASSWORD"));
		if (ObjectUtils.isEmpty(druidPassword)) {

			String password = RandomUtil.randomString(6);
			System.out.println("字典未配置 druid 密码,设置随机密码：" + password + "，如不想有此提示信息，可以在系统配置中添加 [DRUID_PASSWORD] 值");
			servletRegistrationBean.addInitParameter("loginPassword", password);
		} else {
			servletRegistrationBean.addInitParameter("loginPassword", druidPassword.getConfigValue());
		}

		servletRegistrationBean.addInitParameter("resetEnable", "false");
		return servletRegistrationBean;
	}

	@Bean
	public FilterRegistrationBean druidStatFilter() {

		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());

		// 添加过滤规则
		filterRegistrationBean.addUrlPatterns("/*");

		// 添加需要忽略的格式信息
		Map<String, String> initParams = new HashMap<>();
		initParams.put("exclusions", "*.gif,*.jpg,*.png,*.ico,*.js,*.css,/druid/*");

		filterRegistrationBean.setInitParameters(initParams);

		return filterRegistrationBean;

	}
}
