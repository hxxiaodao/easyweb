package com.lyons.easyweb.core.util;

import com.lyons.easyweb.exception.BaseResponseCode;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "返回数据模型")
@Getter
@Setter
public class ResponseData {

	/**
	 * 请求响应code:200表示请求成功,其它表示失败
	 */
	@ApiModelProperty(value = "请求响应code:200为成功,其他为失败")
	private int code;

	/**
	 * 响应客户端的提示
	 */
	@ApiModelProperty(value = "响应消息")
	private String msg;

	@ApiModelProperty(value = "总条数")
	private long count;

	/**
	 * 响应客户端内容
	 */
	@ApiModelProperty(value = "响应内容")
	private Object data;

	public ResponseData() {
	}

	public ResponseData(int code, String msg, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public ResponseData(int code, String msg, Object data, long count) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
		this.count = count;
	}

	public static ResponseData ok(Object data) {
		return new ResponseData(200, "success", data);
	}

	public static ResponseData ok(int code, String msg) {
		return new ResponseData(code, msg, null);
	}

	public static ResponseData ok(long count, Object data) {
		return new ResponseData(200, "success", data, count);
	}

	public static ResponseData ok(int code, Object data) {
		return new ResponseData(code, "success", data);
	}

	public static ResponseData ok(String msg, Object data) {
		return new ResponseData(200, msg, data);
	}

	public static ResponseData ok(int code, String msg, Object data) {
		return new ResponseData(code, msg, data);
	}

	public static ResponseData ok(int code, String msg, Object data, long count) {
		return new ResponseData(code, msg, data, count);
	}

	public static ResponseData fail(String msg) {
		return new ResponseData(0, msg, null);
	}

	public static ResponseData fail(int code, String msg) {
		return new ResponseData(code, msg, null);
	}

	public static ResponseData fail(BaseResponseCode systemError) {
		return new ResponseData(systemError.getCode(), systemError.getMsg(), null);
	}

}
