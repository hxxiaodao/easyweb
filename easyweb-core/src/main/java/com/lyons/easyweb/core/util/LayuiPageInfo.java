package com.lyons.easyweb.core.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LayuiPageInfo {

	private Integer code = 0;
	private String msg = "请求成功";
	private long count;
	private Object data;

	public LayuiPageInfo() {

	}

	public LayuiPageInfo(Object data) {
		super();
		this.data = data;
	}

	public LayuiPageInfo(Integer code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public LayuiPageInfo(Integer code, String msg, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public LayuiPageInfo(Integer code, long count, Object data) {
		super();
		this.code = code;
		this.count = count;
		this.data = data;
	}

	public LayuiPageInfo(Integer code, String msg, long count, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.count = count;
		this.data = data;
	}

	public static LayuiPageInfo ok(long count, Object data) {
		return new LayuiPageInfo(0, "success", count, data);
	}

	public static LayuiPageInfo ok(String msg) {
		return new LayuiPageInfo(0, msg);
	}

	public static LayuiPageInfo ok(Object data) {
		return new LayuiPageInfo(0, "success", data);
	}

	public static LayuiPageInfo ok(String msg, Object data) {
		return new LayuiPageInfo(0, msg, data);
	}

	public static LayuiPageInfo fail(String msg) {
		return new LayuiPageInfo(1, msg);
	}

	public static LayuiPageInfo fail(Integer code, String msg) {
		return new LayuiPageInfo(code, msg);
	}

	public static LayuiPageInfo ok() {
		return new LayuiPageInfo(0, "success");
	}

	public static LayuiPageInfo tongji(String id) {
		// TODO Auto-generated method stub
		return null;
	}

}
