package com.lyons.easyweb.core.util;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import cn.hutool.core.convert.Convert;

@Component
public class RedisOperator {

	@Autowired
	private StringRedisTemplate redisTemplate;

	/**
	 * @description 实现命令：TTL key，以秒为单位，返回给定key的剩余生存时间（TTL，time to live）
	 *
	 */
	public long ttl(String key) {

		return redisTemplate.getExpire(key);
	}

	/**
	 * 实现命令： expire 设置过期时间，单位秒 春晓
	 * 
	 * @description 设置过期时间，单位秒
	 *
	 */
	public void expire(String key, long timeout) {
		redisTemplate.expire(key, timeout, TimeUnit.SECONDS);
	}

	/**
	 * @description 判断指定的key 是否存在【存在=true】
	 */
	public boolean exists(String key) {

		return redisTemplate.hasKey(key);
	}

	public void clearAll() {
		Set<String> keys = redisTemplate.keys("*");
		redisTemplate.delete(keys);
	}

	/**
	 * 
	 * 春晓
	 * 
	 * @description 实现命令：INCR key 增加key 一次
	 *
	 */
	public long incr(String key, long delta) {

		return redisTemplate.opsForValue().increment(key, delta);
	}

	/**
	 * 
	 * 春晓
	 * 
	 * @description KEYS pattern，查找所有符合给定模式的pattern的key
	 *
	 */
	public Set<String> keys(String pattern) {

		return redisTemplate.keys(pattern);
	}

	/**
	 * 
	 * 春晓
	 * 
	 * @description 实现命令：DEL key，删除一个key
	 *
	 */
	public void delete(String key) {
		redisTemplate.delete(key);
	}

	/**
	 * @Version: 0.0.1
	 * @param keys
	 * @return Long 返回成功删除key的数量
	 * @TODO: 批量删除key
	 */
	public Long delete(Collection<String> keys) {
		return redisTemplate.delete(keys);
	}

	/**
	 * 删除指定key
	 * 
	 * @param key
	 * @return
	 */
	public Long flushRedisKey(String key) {

		return Convert.toLong(0);
	}

	/**
	 * 
	 * 实现命令：set key value<br>
	 * 设置一个key-value（将字符串value关联到key）
	 * 
	 * @description
	 *
	 */
	public void set(String key, String value, String className) {
		redisTemplate.opsForValue().set(key, value, 30L, TimeUnit.MINUTES);
	}

	/**
	 * 
	 * 实现命令：set key value ex seconds<br>
	 * 设置key-value和超时时间【秒】
	 * 
	 * @param key
	 * @param value
	 * @param timeout 过期时间（秒为单位）
	 * 
	 * @description
	 *
	 */
	public void set(String key, String value, Long timeout) {

		if (null == timeout) {
			timeout = 86400L;// 24 小时
		}
		redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.MINUTES);
	}

	/**
	 * 
	 * 春晓
	 * 
	 * @description 通过指定的key 获取缓存数据
	 *
	 */
	public String get(String key) {
		return redisTemplate.opsForValue().get(key);
	}

}
