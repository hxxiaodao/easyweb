package com.lyons.easyweb.exception;

public enum BaseResponseCode{
	
    SUCCESS(200,"操作成功"),
    SYSTEM_ERROR(5000,"系统异常请稍后再试"),
    DATA_ID_EMPTY(401,"传入的ID，查无此数据"),
    DATA_ERROR(402,"传入数据异常"),
    DATA_ERROR_WEBSITE_NOT(403,"非法域名，请联系销售!"),
    METHOD_IDENTITY_ERROR(404,"数据校验异常"),
    ACCOUNT_ERROR(405,"该账号不存在"),
    ACCOUNT_LOCK(406,"该账号被锁定,请联系系统管理员"),
    ACCOUNT_PASSWORD_ERROR(407,"用户名密码不匹配"),
    TOKEN_ERROR(400,"用户未登录，请重新登录"),
    TOKEN_NOT_NULL(408,"token 不能为空"),
    SHIRO_AUTHENTICATION_ERROR(409,"用户认证异常"),
    ACCOUNT_HAS_DELETED_ERROR(410,"该账号已被删除，请联系系统管理员"),
    TOKEN_PAST_DUE(412,"token失效,请刷新token"),
    NOT_PERMISSION(413,"没有权限访问该资源"),
    OPERATION_ERROR(414,"操作失败"),
    OPERATION_MENU_PERMISSION_CATALOG_ERROR(415,"操作后的菜单类型是目录，所属菜单必须为默认顶级菜单或者目录"),
    OPERATION_MENU_PERMISSION_MENU_ERROR(416,"操作后的菜单类型是菜单，所属菜单必须为目录类型"),
    OPERATION_MENU_PERMISSION_BTN_ERROR(417,"操作后的菜单类型是按钮，所属菜单必须为菜单类型"),
    OPERATION_MENU_PERMISSION_URL_NOT_NULL(418,"菜单权限的url不能为空"),
    OPERATION_MENU_PERMISSION_URL_PERMS_NULL(419,"菜单权限的标识符不能为空"),
    OPERATION_MENU_PERMISSION_URL_METHOD_NULL(420,"菜单权限的请求方式不能为空"),
    ACCOUNT_LOCK_TIP(421,"该账号被锁定,请联系系统管理员"),
    OPERATION_MENU_PERMISSION_UPDATE(422,"操作的菜单权限存在子集关联不允许变更"),
    ROLE_PERMISSION_RELATION(423, "该菜单权限存在子集关联，不允许删除"),
    NOT_PERMISSION_DELETED_DEPT(424,"该组织机构下还关联着用户，不允许删除"),
    OLD_PASSWORD_ERROR(425,"旧密码不匹配"),
    OPERATION_MENU_PERMISSION_URL_CODE_NULL(426,"菜单权限的按钮标识不能为空"),
    UPLOAD_FILE_ERROR(427,"上传失败"),
    FILE_TOO_LARGE(428,"上传的文件超出范围"),
    ;

    /**
     *  响应码
     */
    private int code;

    /**
     * 提示
     */
    private String msg;

    BaseResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
