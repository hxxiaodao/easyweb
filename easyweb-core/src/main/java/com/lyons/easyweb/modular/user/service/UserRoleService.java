package com.lyons.easyweb.modular.user.service;

import com.lyons.easyweb.modular.user.pojo.UserRole;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户与角色关系 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface UserRoleService extends IService<UserRole> {

	/**
	 * 用户角色关联
	 * 
	 * @param userid 用户ID
	 * @param select 选中的角色
	 */
	void saveUserRole(String userid, String[] select);

	/**
	 * 查询用户就拥有的角色
	 * 
	 * @param userId
	 * @return
	 */
	List<Map<String, String>> querUserRoleByUserId(String userId);

}
