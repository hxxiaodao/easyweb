package com.lyons.easyweb.modular.fileInfo.service;

import com.lyons.easyweb.modular.fileInfo.pojo.FileInfo;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文件信息表 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface FileInfoService extends IService<FileInfo> {

	/**
	 * 文件上传返回文件的绝对路径
	 * 
	 * @param file
	 * @return
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	String upload(MultipartFile file) throws IllegalStateException, IOException;

	List<FileInfo> listAll();

}
