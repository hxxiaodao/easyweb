package com.lyons.easyweb.modular.notice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyons.easyweb.modular.notice.pojo.Notice;

/**
 * <p>
 * 通知表 服务类
 * </p>
 *
 * @author HengDuCms
 * @since 2021-06-28
 */
public interface NoticeService extends IService<Notice> {

	/**
	 * 添加公告
	 * 
	 * @param title
	 * @param content
	 */
	void save(String title, String content);

	void save(String title, String content, String suId);

}
