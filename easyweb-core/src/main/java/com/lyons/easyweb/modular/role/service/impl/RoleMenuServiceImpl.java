package com.lyons.easyweb.modular.role.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.lyons.easyweb.modular.role.dao.RoleMenuMapper;
import com.lyons.easyweb.modular.role.pojo.RoleMenu;
import com.lyons.easyweb.modular.role.service.RoleMenuService;

/**
 * <p>
 * 角色与菜单对应关系 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

	@Override
	public void insertRoleMenu(RoleMenu roleMenu) {

		// 移除旧角色菜单数据
		this.remove(new QueryWrapper<RoleMenu>().lambda().eq(RoleMenu::getRoleId, roleMenu.getRoleId()));

		List<RoleMenu> list = Lists.newArrayList();

		if (!ObjectUtils.isEmpty(roleMenu.getMids())) {

			List<String> item = Arrays.asList(roleMenu.getMids().split("&"));
			item.forEach(e -> {
				
				RoleMenu bean = RoleMenu.builder().roleId(roleMenu.getRoleId()).menuId(e).build();
				list.add(bean);
				
			});
		}
		if (list.size() > 0) {
			this.saveBatch(list);
		}
	}

}
