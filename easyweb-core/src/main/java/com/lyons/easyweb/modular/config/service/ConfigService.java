package com.lyons.easyweb.modular.config.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.modular.config.pojo.Config;

/**
 * <p>
 * 参数配置 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface ConfigService extends IService<Config> {

	LayuiPageInfo delete(String id);

	LayuiPageInfo batchDelete(String ids);

	LayuiPageInfo deleteAll();

	LayuiPageInfo insertOrUpdate(Config config);

	/**
	 * 根据可以获得value属性值
	 * 
	 * @param code
	 * @return
	 */
	public String queryConfigValue(String code);

	/**
	 * 获取上传文件保存位置
	 * 
	 * @return
	 */
	public String getFilePath();
	
	public String getStaticFilePath();
	
}
