package com.lyons.easyweb.modular.role.dao;

import com.lyons.easyweb.modular.role.pojo.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色与菜单对应关系 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}
