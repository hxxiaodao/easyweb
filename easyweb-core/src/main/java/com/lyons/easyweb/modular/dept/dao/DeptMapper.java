package com.lyons.easyweb.modular.dept.dao;

import com.lyons.easyweb.modular.dept.pojo.Dept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门信息 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface DeptMapper extends BaseMapper<Dept> {

}
