package com.lyons.easyweb.modular.log.service;

import com.lyons.easyweb.modular.log.pojo.ErrorLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 错误信息 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface ErrorLogService extends IService<ErrorLog> {

}
