package com.lyons.easyweb.modular.user.dao;

import com.lyons.easyweb.modular.user.pojo.UserRole;

import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户与角色关系 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

	/**
	 * 查询用户拥有的角色
	 * 
	 * @param userId
	 * @return
	 */
	List<Map<String, String>> querUserRoleByUserId(@Param("userId") String userId);

}
