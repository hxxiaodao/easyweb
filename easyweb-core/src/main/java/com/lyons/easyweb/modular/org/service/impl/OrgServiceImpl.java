package com.lyons.easyweb.modular.org.service.impl;

import com.lyons.easyweb.modular.org.pojo.Org;
import com.lyons.easyweb.modular.org.dao.OrgMapper;
import com.lyons.easyweb.modular.org.service.OrgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统组织机构表 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class OrgServiceImpl extends ServiceImpl<OrgMapper, Org> implements OrgService {

}
