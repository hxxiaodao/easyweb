package com.lyons.easyweb.modular.notice.pojo;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>
 * 通知表
 * </p>
 *
 * @author HengDuCms
 * @since 2021-06-28
 */
@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("sys_notice")
public class Notice extends Model<Notice> {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 通知发送者
	 */
	@TableField(value = "suId", insertStrategy = FieldStrategy.NOT_EMPTY, updateStrategy = FieldStrategy.NOT_EMPTY)
	private String suid;

	/**
	 * 通知接收者
	 */
	@TableField(value = "reId", insertStrategy = FieldStrategy.NOT_EMPTY, updateStrategy = FieldStrategy.NOT_EMPTY)
	private String reid;

	/**
	 * 阅读状态[0-已阅读;1-未阅读]
	 */
	@TableField("read_status")
	private Integer readStatus;

	/**
	 * 标题
	 */
	@NotBlank(message = "标题不能够为空")
	@TableField("title")
	private String title;

	/**
	 * 内容
	 */
	@TableField("content")
	private String content;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 更新人
	 */
	@TableField("update_user")
	private String updateUser;


}
