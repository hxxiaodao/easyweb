package com.lyons.easyweb.modular.fileInfo.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 文件信息表
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("sys_file_info")
public class FileInfo extends Model<FileInfo> {


    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 文件存储位置[1:阿里云，2:腾讯云，3:minio，4:本地]
     */
    @TableField("file_location")
    private Integer fileLocation;

    /**
     * 文件仓库
     */
    @TableField("file_bucket")
    private String fileBucket;

    /**
     * 文件名称[上传时候的文件名]
     */
    @TableField("file_origin_name")
    private String fileOriginName;

    /**
     * 文件后缀
     */
    @TableField("file_suffix")
    private String fileSuffix;

    /**
     * 文件大小kb
     */
    @TableField("file_size_kb")
    private String fileSizeKb;

    /**
     * 文件大小信息，计算后的
     */
    @TableField("file_size_info")
    private String fileSizeInfo;

    /**
     * 存储到bucket的名称[文件唯一标识id]
     */
    @TableField("file_object_name")
    private String fileObjectName;

    /**
     * 存储路径
     */
    @TableField("file_path")
    private String filePath;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 创建用户
     */
    @TableField("create_user")
    private String createUser;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 修改用户
     */
    @TableField("update_user")
    private String updateUser;


}
