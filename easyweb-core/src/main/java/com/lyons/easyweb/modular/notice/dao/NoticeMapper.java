package com.lyons.easyweb.modular.notice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyons.easyweb.modular.notice.pojo.Notice;

/**
 * <p>
 * 通知表 Mapper 接口
 * </p>
 *
 * @author HengDuCms
 * @since 2021-06-28
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
