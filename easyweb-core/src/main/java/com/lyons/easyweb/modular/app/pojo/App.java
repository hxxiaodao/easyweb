package com.lyons.easyweb.modular.app.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>
 * 系统应用表
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = false)
@TableName("sys_app")
public class App extends Model<App> {

	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 应用名称
	 */
	@TableField("name")
	private String name;

	/**
	 * 编码
	 */
	@TableField("code")
	private String code;

	/**
	 * 排序,大靠前
	 */
	@TableField("sort")
	private Integer sort;

	/**
	 * 删除标记：0-已删除，1-未删除
	 */
	@TableField("del_flag")
	private Integer delFlag;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 更新人
	 */
	@TableField("update_user")
	private String updateUser;

	/**
	 * 状态[0-禁用;1-激活;]
	 */
	@TableField("status")
	private Integer status;
}
