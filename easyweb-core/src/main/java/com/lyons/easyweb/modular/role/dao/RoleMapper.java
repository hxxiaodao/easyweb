package com.lyons.easyweb.modular.role.dao;

import com.lyons.easyweb.modular.role.pojo.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface RoleMapper extends BaseMapper<Role> {

}
