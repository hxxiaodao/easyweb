package com.lyons.easyweb.modular.timer.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyons.easyweb.modular.timer.pojo.Timers;

/**
 * <p>
 * 定时任务 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-12
 */
public interface TimersService extends IService<Timers> {

	public List<String> getActionClasses();

	public boolean status(String id, boolean status);

	public void insertOrUpdate(Timers timers);
}
