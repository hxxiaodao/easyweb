package com.lyons.easyweb.modular.user.vo.req;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class UserReqVo implements Serializable {

	@NotBlank(message = "登录账号不能够为空 !")
	private String account;

	@NotBlank(message = "登录密码不能够为空 !")
	private String password;
}
