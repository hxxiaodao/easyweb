package com.lyons.easyweb.modular.pos.dao;

import com.lyons.easyweb.modular.pos.pojo.Pos;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统职位表 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface PosMapper extends BaseMapper<Pos> {

}
