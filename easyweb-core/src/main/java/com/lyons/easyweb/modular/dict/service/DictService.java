package com.lyons.easyweb.modular.dict.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.modular.dict.pojo.Dict;

/**
 * <p>
 * 字典 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface DictService extends IService<Dict> {

	/**
	 * 
	 * @param id
	 * @return
	 */
	LayuiPageInfo deleteById(String id);

	LayuiPageInfo batchDelete(String ids);

	LayuiPageInfo deleteAll();

	void insertOrUpdate(Dict dict);

}
