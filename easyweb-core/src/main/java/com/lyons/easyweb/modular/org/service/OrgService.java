package com.lyons.easyweb.modular.org.service;

import com.lyons.easyweb.modular.org.pojo.Org;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统组织机构表 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface OrgService extends IService<Org> {

}
