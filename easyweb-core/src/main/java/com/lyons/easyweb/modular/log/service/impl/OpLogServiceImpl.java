package com.lyons.easyweb.modular.log.service.impl;

import com.lyons.easyweb.modular.log.pojo.OpLog;
import com.lyons.easyweb.modular.log.dao.OpLogMapper;
import com.lyons.easyweb.modular.log.service.OpLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统操作日志表 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class OpLogServiceImpl extends ServiceImpl<OpLogMapper, OpLog> implements OpLogService {

}
