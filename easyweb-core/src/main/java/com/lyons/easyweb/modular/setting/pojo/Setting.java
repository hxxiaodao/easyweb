package com.lyons.easyweb.modular.setting.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统设置
 * </p>
 *
 * @author Lyons
 * @since 2021-07-05
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_setting")
public class Setting extends Model<Setting> {

	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 首页标题
	 */
	@TableField("title")
	private String title;

	/**
	 * 网站名称
	 */
	@TableField("sitename")
	private String sitename;

	/**
	 * 域名
	 */
	@TableField("domain")
	private String domain;

	/**
	 * 缓存时间,单位分钟
	 */
	@TableField("cache_time")
	private Integer cacheTime;

	/**
	 * 最大文件上传,单位KB
	 */
	@TableField("update_file_size")
	private Integer updateFileSize;

	/**
	 * 上传文件类型(png|gif|jpg|jpeg|zip|rar)
	 */
	@TableField("update_file_type")
	private String updateFileType;

	/**
	 * META 关键词
	 */
	@TableField("keywords")
	private String keywords;

	/**
	 * META 描述,多个词用逗号分隔
	 */
	@TableField("descript")
	private String descript;

	/**
	 * 主词
	 */
	@TableField("main_key")
	private String mainKey;

	/**
	 * 备案信息
	 */
	@TableField("icp")
	private String icp;

	/**
	 * 版权信息
	 */
	@TableField("copyright")
	private String copyright;

	/**
	 * 网站图标
	 */
	@TableField("icon")
	private String icon;

	/**
	 * 网站logo
	 */
	@TableField("logo")
	private String logo;

	/**
	 * 版本
	 */
	@TableField("version")
	private String version;

	/**
	 * 网站备注/描述
	 */
	@TableField("remark")
	private String remark;

	/**
	 * 创建加时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 更新人
	 */
	@TableField("update_user")
	private String updateUser;


}
