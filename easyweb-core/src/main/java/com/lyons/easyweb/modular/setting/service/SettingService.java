package com.lyons.easyweb.modular.setting.service;

import com.lyons.easyweb.modular.setting.pojo.Setting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统设置 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-05
 */
public interface SettingService extends IService<Setting> {

	Setting querySettingInfo();

}
