package com.lyons.easyweb.modular.menu.pojo;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 菜单管理
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("sys_menu")
public class Menu extends Model<Menu> {

	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 父菜单ID，一级菜单为0
	 */
	@TableField(value = "pid", insertStrategy = FieldStrategy.NOT_EMPTY, updateStrategy = FieldStrategy.NOT_EMPTY)
	private String pid;

	/**
	 * 父ids
	 */
	@TableField(value = "pids", insertStrategy = FieldStrategy.NOT_EMPTY, updateStrategy = FieldStrategy.NOT_EMPTY)
	private String pids;

	/**
	 * 菜单名称
	 */
	@NotBlank(message = "名称必填 !")
	@TableField("name")
	private String name;

	/**
	 * 编码
	 */
	@TableField("code")
	private String code;

	/**
	 * 路由地址
	 */
	@TableField("router")
	private String router;

	/**
	 * 打开方式
	 */
	@TableField("target")
	private String target;

	/**
	 * 授权(多个用逗号分隔，如：user:list,user:create)
	 */
	@TableField("perms")
	private String perms;

	/**
	 * 类型 0：目录 1：菜单 2：按钮
	 */
	@TableField("type")
	private Integer type;

	/**
	 * 菜单图标
	 */
	@TableField("icon")
	private String icon;

	/**
	 * 应用分类[应用编码]
	 */
	@TableField("application")
	private String application;

	/**
	 * 权重[字典 1系统权重 2业务权重]
	 */
	@TableField("weight")
	private Integer weight;

	/**
	 * 描述
	 */
	@TableField("remark")
	private String remark;

	/**
	 * 排序[值越大越靠前]
	 */
	@TableField("sort")
	private Integer sort;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 状态[0-禁用；1- 激活]
	 */
	@TableField("status")
	private Integer status;


}
