package com.lyons.easyweb.modular.user.vo.resp;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginRespVo implements Serializable{

	// 用户认证凭证
	private String token;

	// 用户id
	private String userId;
}
