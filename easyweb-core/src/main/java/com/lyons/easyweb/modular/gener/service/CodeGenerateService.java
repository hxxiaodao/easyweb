package com.lyons.easyweb.modular.gener.service;

import com.lyons.easyweb.modular.gener.pojo.CodeGenerate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 代码生成基础配置 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface CodeGenerateService extends IService<CodeGenerate> {

}
