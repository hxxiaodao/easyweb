package com.lyons.easyweb.modular.menu.vo.resp;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@JsonInclude(value = Include.NON_EMPTY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MenuVoResp implements Serializable {

	private String id;

	private String pid;

	private String name;
	
	private Boolean selected;

	private List<MenuVoResp> children;

}
