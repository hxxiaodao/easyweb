package com.lyons.easyweb.modular.app.dao;

import com.lyons.easyweb.modular.app.pojo.App;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统应用表 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface AppMapper extends BaseMapper<App> {

}
