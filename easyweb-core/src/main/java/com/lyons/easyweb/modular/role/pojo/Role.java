package com.lyons.easyweb.modular.role.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role")
public class Role extends Model<Role> {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 角色名称
	 */
	@TableField("name")
	private String name;

	/**
	 * 角色编码
	 */
	private String code;

	/**
	 * 排序
	 */
	private Integer sort;

	/**
	 * 数据范围 [1全部数据 2本部门及以下数据 3本部门数据 4仅本人数据 5自定义数据]'
	 */
	@TableField("data_scope_type")
	private Integer dataScopeType;

	/**
	 * 描述
	 */
	@TableField("remark")
	private String remark;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 是否删除(1未删除；0已删除)
	 */
	@TableField("del_flag")
	private Integer delFlag;

	/**
	 * 状态(1:正常0:弃用)
	 */
	@TableField("status")
	private Integer status;

}
