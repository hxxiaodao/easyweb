package com.lyons.easyweb.modular.menu.pojo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@JsonInclude(value = Include.NON_NULL)
@Data
@NoArgsConstructor
public class MenuVo implements Serializable {

	private String id;

	private String pid;

	private String title;

	private String icon;

	private String href;

	private String target;

	private List<MenuVo> child;
}
