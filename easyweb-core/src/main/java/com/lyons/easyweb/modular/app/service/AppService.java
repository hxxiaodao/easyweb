package com.lyons.easyweb.modular.app.service;

import com.lyons.easyweb.modular.app.pojo.App;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统应用表 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface AppService extends IService<App> {

}
