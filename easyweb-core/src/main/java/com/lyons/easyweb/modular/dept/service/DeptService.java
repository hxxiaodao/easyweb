package com.lyons.easyweb.modular.dept.service;

import com.lyons.easyweb.modular.dept.pojo.Dept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 部门信息 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface DeptService extends IService<Dept> {

}
