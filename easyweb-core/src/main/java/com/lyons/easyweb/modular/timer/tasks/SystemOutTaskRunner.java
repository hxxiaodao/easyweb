package com.lyons.easyweb.modular.timer.tasks;

import org.springframework.stereotype.Component;

import com.lyons.easyweb.modular.timer.service.TimerTaskRunner;

import cn.hutool.core.date.DateUtil;

/**
 * 这是一个定时任务的示例程序
 *
 */
@Component
public class SystemOutTaskRunner implements TimerTaskRunner {

	@Override
	public void action() {
		System.out.println("改革吹风吹....." + DateUtil.date());
	}

}
