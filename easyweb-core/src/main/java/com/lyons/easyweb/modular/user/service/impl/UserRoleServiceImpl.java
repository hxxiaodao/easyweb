package com.lyons.easyweb.modular.user.service.impl;

import com.lyons.easyweb.modular.user.pojo.UserRole;
import com.lyons.easyweb.modular.user.dao.UserRoleMapper;
import com.lyons.easyweb.modular.user.service.UserRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 * 用户与角色关系 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

	@Override
	public void saveUserRole(String userid, String[] select) {

		// 删除用户就角色
		remove(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUserId, userid));

		// 添加新角色
		if (!ObjectUtils.isEmpty(select)) {

			List<UserRole> list = Lists.newArrayList();
			for (String uid : select) {

				UserRole userRole = UserRole.builder().userId(userid).roleId(uid).createTime(new Date()).build();
				list.add(userRole);
			}
			saveBatch(list);
		}
	}

	@Override
	public List<Map<String, String>> querUserRoleByUserId(String userId) {
		
		
		return baseMapper.querUserRoleByUserId(userId);
	}

}
