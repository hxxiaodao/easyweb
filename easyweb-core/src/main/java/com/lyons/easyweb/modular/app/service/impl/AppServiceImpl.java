package com.lyons.easyweb.modular.app.service.impl;

import com.lyons.easyweb.modular.app.pojo.App;
import com.lyons.easyweb.modular.app.dao.AppMapper;
import com.lyons.easyweb.modular.app.service.AppService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统应用表 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class AppServiceImpl extends ServiceImpl<AppMapper, App> implements AppService {

}
