package com.lyons.easyweb.modular.gener.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 代码生成详细配置
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_code_generate_config")
public class CodeGenerateConfig extends Model<CodeGenerateConfig> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 代码生成主表ID
     */
    @TableField("code_gen_id")
    private Long codeGenId;

    /**
     * 数据库字段名
     */
    @TableField("column_name")
    private String columnName;

    /**
     * java类字段名
     */
    @TableField("java_name")
    private String javaName;

    /**
     * 物理类型
     */
    @TableField("data_type")
    private String dataType;

    /**
     * 字段描述
     */
    @TableField("column_comment")
    private String columnComment;

    /**
     * java类型
     */
    @TableField("java_type")
    private String javaType;

    /**
     * 作用类型（字典）
     */
    @TableField("effect_type")
    private String effectType;

    /**
     * 字典code
     */
    @TableField("dict_type_code")
    private String dictTypeCode;

    /**
     * 列表展示
     */
    @TableField("whether_table")
    private String whetherTable;

    /**
     * 增改
     */
    @TableField("whether_add_update")
    private String whetherAddUpdate;

    /**
     * 列表是否缩进（字典）
     */
    @TableField("whether_retract")
    private String whetherRetract;

    /**
     * 是否必填（字典）
     */
    @TableField("whether_required")
    private String whetherRequired;

    /**
     * 是否是查询条件
     */
    @TableField("query_whether")
    private String queryWhether;

    /**
     * 查询方式
     */
    @TableField("query_type")
    private String queryType;

    /**
     * 主键
     */
    @TableField("column_key")
    private String columnKey;

    /**
     * 主外键名称
     */
    @TableField("column_key_name")
    private String columnKeyName;

    /**
     * 是否是通用字段
     */
    @TableField("whether_common")
    private String whetherCommon;

    /**
     * 域名添加时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 创建人
     */
    @TableField("create_user")
    private Long createUser;

    /**
     * 更新人
     */
    @TableField("update_user")
    private Long updateUser;


}
