package com.lyons.easyweb.modular.fileInfo.dao;

import com.lyons.easyweb.modular.fileInfo.pojo.FileInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件信息表 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface FileInfoMapper extends BaseMapper<FileInfo> {

}
