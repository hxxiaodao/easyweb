package com.lyons.easyweb.modular.role.service.impl;

import com.lyons.easyweb.modular.role.pojo.Role;
import com.lyons.easyweb.modular.role.dao.RoleMapper;
import com.lyons.easyweb.modular.role.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
