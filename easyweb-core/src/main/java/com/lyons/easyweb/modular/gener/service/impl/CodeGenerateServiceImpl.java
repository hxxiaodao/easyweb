package com.lyons.easyweb.modular.gener.service.impl;

import com.lyons.easyweb.modular.gener.pojo.CodeGenerate;
import com.lyons.easyweb.modular.gener.dao.CodeGenerateMapper;
import com.lyons.easyweb.modular.gener.service.CodeGenerateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 代码生成基础配置 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class CodeGenerateServiceImpl extends ServiceImpl<CodeGenerateMapper, CodeGenerate> implements CodeGenerateService {

}
