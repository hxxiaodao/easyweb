package com.lyons.easyweb.modular.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyons.easyweb.modular.user.pojo.SysUser;

/**
 * <p>
 * 系统用户 Mapper 接口
 * </p>
 *
 * @author HengDuCms
 * @since 2021-06-27
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
