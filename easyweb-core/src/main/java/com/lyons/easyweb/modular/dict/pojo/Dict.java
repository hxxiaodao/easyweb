package com.lyons.easyweb.modular.dict.pojo;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 字典
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dict")
public class Dict extends Model<Dict> {

	/**
	 * 字典id
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 上级字典的id(如果没有上级字典id，则为0)
	 */
	@TableField("pid")
	private String pid;

	/**
	 * 字典名称
	 */
	@NotBlank(message = "字典名不能为空")
	@TableField("dict_name")
	private String dictName;

	/**
	 * 字典编码
	 */
	@NotBlank(message = "字典编码不能够为为空")
	@TableField("dict_code")
	private String dictCode;

	/**
	 * 字典类型（1-系统类型，2-业务类型）
	 */
	@TableField("dict_type")
	private Integer dictType;

	/**
	 * 排序，大靠前
	 */
	@TableField("sort")
	private Integer sort;

	/**
	 * 是否删除，Y-被删除，N-未删除
	 */
	@TableField("del_flag")
	private String delFlag;

	/**
	 * 备注
	 */
	@TableField("remark")
	private String remark;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 创建用户id
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 修改时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 修改用户id
	 */
	@TableField("update_user")
	private String updateUser;

	/**
	 * 状态[0-禁用;1-激活;]
	 */
	@TableField("status")
	private Integer status;

}
