package com.lyons.easyweb.modular.pos.pojo;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统职位表
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_pos")
public class Pos extends Model<Pos> {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 名称
	 */
	@NotBlank(message = "职位名称不能够为空 !")
	@TableField("name")
	private String name;

	/**
	 * 编码
	 */
	@NotBlank(message = "职位编码,不能够为空 !")
	@TableField("code")
	private String code;

	/**
	 * 排序
	 */
	@TableField("sort")
	private Integer sort;

	/**
	 * 备注
	 */
	@TableField("remark")
	private String remark;

	/**
	 * 状态（字典 0正常 1停用 2删除）
	 */
	@TableField("status")
	private Integer status;

	/**
	 * 域名添加时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 更新人
	 */
	@TableField("update_user")
	private String updateUser;

}
