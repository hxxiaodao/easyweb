package com.lyons.easyweb.modular.config.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyons.easyweb.core.util.LayuiPageInfo;
import com.lyons.easyweb.core.util.StringUtils;
import com.lyons.easyweb.modular.config.dao.ConfigMapper;
import com.lyons.easyweb.modular.config.pojo.Config;
import com.lyons.easyweb.modular.config.service.ConfigService;

/**
 * <p>
 * 参数配置 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Lazy
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, Config> implements ConfigService {

	@Value("${spring.application.name}")
	private String applicatonName;

	@Override
	public LayuiPageInfo delete(String id) {

		Config config = this.getById(id);
		if (config.getSysFlag().equalsIgnoreCase("Y")) {

			return LayuiPageInfo.fail("系统参数不能够删除 !");
		}

		return null;
	}

	@Override
	public LayuiPageInfo batchDelete(String ids) {

		List<String> idList = Arrays.asList(ids.split(",")).stream().map(e -> {
			if (!ObjectUtils.isEmpty(e)) {
				return e;
			}
			return null;
		}).collect(Collectors.toList());

		List<Config> list = this.listByIds(idList);
		int k = 0;
		for (Config config : list) {

			if (config.getSysFlag().equalsIgnoreCase("N")) {
				k++;
				this.removeById(config.getId());
			}
		}

		return LayuiPageInfo.ok("成功删除了 " + k + " 条业务参数 !");
	}

	@Override
	public LayuiPageInfo deleteAll() {

		int k = 0;
		List<Config> list = this.list();

		for (Config config : list) {

			if (config.getSysFlag().equalsIgnoreCase("N")) {
				k++;
				this.removeById(config.getId());
			}
		}
		return LayuiPageInfo.ok("成功删除了 " + k + " 条业务参数 !");
	}

	@Override
	public LayuiPageInfo insertOrUpdate(Config config) {

		this.saveOrUpdate(config);

//		if (ObjectUtils.isEmpty(config.getId())) {
//
//			this.save(config);
//		} else {
//
//			Config oldConfig = this.getById(config.getId());
//			if (oldConfig.getSysFlag().equalsIgnoreCase("Y")) {
//
//				return LayuiPageInfo.fail("系统参数无法修改 !");
//			}
//			updateById(config);
//		}

		return LayuiPageInfo.ok("系统参数修改成功 !");
	}

	@Override
	public String queryConfigValue(String code) {

		Config config = baseMapper.selectOne(new LambdaQueryWrapper<Config>().eq(Config::getConfigCode, code));
		return config.getConfigValue();
	}

	@Override
	public String getFilePath() {

		String path = "";

		// 获得系统设置的文件保存路径
		if (StringUtils.systemInfo().equals("linux")) {

			Config config = this.getOne(new QueryWrapper<Config>().lambda().eq(Config::getConfigCode, "LOCAL_FILE_SAVE_PATH_LINUX"));
			if (ObjectUtils.isEmpty(config)) {

				config = Config.builder().configName("Linux本地文件保存路径").configCode("LOCAL_FILE_SAVE_PATH_LINUX").configValue("/temp/" + applicatonName + "/defaultBucket").sysFlag("Y").sort(100).delFlag("N").build();
				this.save(config);
			}
			path = config.getConfigValue();
		} else {

			Config config = this.getOne(new QueryWrapper<Config>().lambda().eq(Config::getConfigCode, "LOCAL_FILE_SAVE_PATH_WINDOWS"));
			if (ObjectUtils.isEmpty(config)) {
				config = Config.builder().configName("Windows本地文件保存路径").configCode("LOCAL_FILE_SAVE_PATH_WINDOWS").configValue("d:/tmp/" + applicatonName + "/defaultBucket").sysFlag("Y").sort(100).delFlag("N").build();
				this.save(config);
			}
			path = config.getConfigValue();
		}
		return path;
	}

	@Override
	public String getStaticFilePath() {
		String path = "";

		// 获得系统设置的文件保存路径
		if (StringUtils.systemInfo().equals("linux")) {

			Config config = this.getOne(new QueryWrapper<Config>().lambda().eq(Config::getConfigCode, "STATIC_PATH_LINUX"));
			if (ObjectUtils.isEmpty(config)) {

				config = Config.builder().configName("Linux静态文件保存路径").configCode("STATIC_PATH_LINUX").configValue("/temp/" + applicatonName + "/defaultBucket").sysFlag("Y").sort(100).delFlag("N").build();
				this.save(config);
			}
			path = config.getConfigValue();
		} else {

			Config config = this.getOne(new QueryWrapper<Config>().lambda().eq(Config::getConfigCode, "STATIC_PATH_WINDOWS"));
			if (ObjectUtils.isEmpty(config)) {
				config = Config.builder().configName("Windows静态保存路径").configCode("STATIC_PATH_WINDOWS").configValue("d:/tmp/" + applicatonName + "/defaultBucket").sysFlag("Y").sort(100).delFlag("N").build();
				this.save(config);
			}
			path = config.getConfigValue();
		}
		return path;
	}

}
