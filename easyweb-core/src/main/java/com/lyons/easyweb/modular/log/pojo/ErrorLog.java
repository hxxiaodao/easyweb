package com.lyons.easyweb.modular.log.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 错误信息
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_error_log")
public class ErrorLog extends Model<ErrorLog> {


    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    /**
     * 设备信息[1-WEB;2-App]
     */
    @TableField("device")
    private Integer device;

    /**
     * 错误类名
     */
    @TableField("class_name")
    private String className;

    /**
     * 错误描述
     */
    @TableField("error_msg")
    private String errorMsg;

    /**
     * 错误详细信息
     */
    @TableField("exception")
    private String exception;

    /**
     * 请求地址
     */
    @TableField("url")
    private String url;

    /**
     * 请求方法
     */
    @TableField("method")
    private String method;

    /**
     * 参数
     */
    @TableField("param")
    private String param;

    /**
     * 用户IP
     */
    @TableField("ip")
    private String ip;

    /**
     * 日志产生时间
     */
    @TableField("create_time")
    private Date createTime;


}
