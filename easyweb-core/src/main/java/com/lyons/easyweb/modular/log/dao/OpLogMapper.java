package com.lyons.easyweb.modular.log.dao;

import com.lyons.easyweb.modular.log.pojo.OpLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统操作日志表 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface OpLogMapper extends BaseMapper<OpLog> {

}
