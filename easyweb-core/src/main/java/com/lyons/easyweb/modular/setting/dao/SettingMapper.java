package com.lyons.easyweb.modular.setting.dao;

import com.lyons.easyweb.modular.setting.pojo.Setting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统设置 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-05
 */
public interface SettingMapper extends BaseMapper<Setting> {

}
