package com.lyons.easyweb.modular.gener.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 代码生成基础配置
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_code_generate")
public class CodeGenerate extends Model<CodeGenerate> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 作者姓名
     */
    @TableField("author_name")
    private String authorName;

    /**
     * 类名
     */
    @TableField("class_name")
    private String className;

    /**
     * 是否移除表前缀
     */
    @TableField("table_prefix")
    private String tablePrefix;

    /**
     * 生成位置类型
     */
    @TableField("generate_type")
    private String generateType;

    /**
     * 数据库表名
     */
    @TableField("table_name")
    private String tableName;

    /**
     * 包名称
     */
    @TableField("package_name")
    private String packageName;

    /**
     * 业务名
     */
    @TableField("bus_name")
    private String busName;

    /**
     * 功能名
     */
    @TableField("table_comment")
    private String tableComment;

    /**
     * 域名添加时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 创建人
     */
    @TableField("create_user")
    private Long createUser;

    /**
     * 更新人
     */
    @TableField("update_user")
    private Long updateUser;


}
