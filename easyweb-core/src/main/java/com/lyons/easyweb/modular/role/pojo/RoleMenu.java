package com.lyons.easyweb.modular.role.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 角色与菜单对应关系
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role_menu")
public class RoleMenu extends Model<RoleMenu> {
	
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 角色ID
	 */
	@TableField("role_id")
	private String roleId;

	/**
	 * 角色选中的菜单ID集合
	 */
	@TableField(exist = false)
	private String mids;

	/**
	 * 菜单ID
	 */
	@TableField("menu_id")
	private String menuId;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 更新人
	 */
	@TableField("update_user")
	private String updateUser;

}
