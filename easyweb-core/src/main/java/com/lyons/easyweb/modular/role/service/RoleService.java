package com.lyons.easyweb.modular.role.service;

import com.lyons.easyweb.modular.role.pojo.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface RoleService extends IService<Role> {

}
