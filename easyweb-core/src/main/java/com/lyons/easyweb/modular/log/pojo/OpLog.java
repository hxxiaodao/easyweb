package com.lyons.easyweb.modular.log.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统操作日志表
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_op_log")
public class OpLog extends Model<OpLog> {


    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 操作类型
     */
    @TableField("op_type")
    private Integer opType;

    /**
     * 是否执行成功[Y-是，N-否]
     */
    @TableField("success")
    private String success;

    /**
     * 具体消息
     */
    @TableField("message")
    private String message;

    /**
     * ip
     */
    @TableField("ip")
    private String ip;

    /**
     * 地址
     */
    @TableField("location")
    private String location;

    /**
     * 浏览器
     */
    @TableField("browser")
    private String browser;

    /**
     * 操作系统
     */
    @TableField("os")
    private String os;

    /**
     * 请求地址
     */
    @TableField("url")
    private String url;

    /**
     * 类名称
     */
    @TableField("class_name")
    private String className;

    /**
     * 方法名称
     */
    @TableField("method_name")
    private String methodName;

    /**
     * 请求方式[GET POST PUT DELETE)
     */
    @TableField("req_method")
    private String reqMethod;

    /**
     * 请求参数
     */
    @TableField("param")
    private String param;

    /**
     * 返回结果
     */
    @TableField("result")
    private String result;

    /**
     * 操作时间
     */
    @TableField("op_time")
    private Date opTime;

    /**
     * 操作账号
     */
    @TableField("account")
    private String account;



}
