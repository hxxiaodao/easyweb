package com.lyons.easyweb.modular.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyons.easyweb.modular.user.pojo.SysUser;
import com.lyons.easyweb.modular.user.vo.req.UserReqVo;
import com.lyons.easyweb.modular.user.vo.resp.LoginRespVo;

/**
 * <p>
 * 系统用户 服务类
 * </p>
 *
 * @author HengDuCms
 * @since 2021-06-27
 */
public interface UserService extends IService<SysUser> {

	LoginRespVo login(UserReqVo userReqVo);

	boolean updatePwd(String oldPassword, String newPassword, String userId);

	/**
	 * 用户重置密码
	 * 
	 * @param userId
	 */
	void restPassword(String userId);

}
