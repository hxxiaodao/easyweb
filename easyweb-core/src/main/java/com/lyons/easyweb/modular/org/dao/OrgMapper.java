package com.lyons.easyweb.modular.org.dao;

import com.lyons.easyweb.modular.org.pojo.Org;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统组织机构表 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface OrgMapper extends BaseMapper<Org> {

}
