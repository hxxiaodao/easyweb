package com.lyons.easyweb.modular.menu.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyons.easyweb.modular.menu.pojo.Menu;
import com.lyons.easyweb.modular.menu.pojo.TreeModel;
import com.lyons.easyweb.modular.menu.vo.resp.MenuVoResp;

/**
 * <p>
 * 菜单管理 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface MenuService extends IService<Menu> {

	/**
	 * 
	 * 查询所有的菜单数据（layui mini 显示菜单使用）
	 * 
	 * @return
	 */
	Map<String, Object> listMenu(String userId);

	/**
	 * ztree 查询菜单数据（添加菜单或者角色授权菜单使用）.roleId 为空查询所有的菜单，roleId不为空，者角色选中的菜单默认为选中状态
	 * 
	 * @param roleId 角色ID
	 * @return
	 */
	List<TreeModel> queryAllModules(String roleId);

	/**
	 * xm-select 查询菜单树
	 * 
	 * @param roleId
	 * @param mid 
	 * @return
	 */
	List<MenuVoResp> queryMenuList(String roleId, String mid);

}
