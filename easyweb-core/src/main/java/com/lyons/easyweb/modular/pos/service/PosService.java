package com.lyons.easyweb.modular.pos.service;

import com.lyons.easyweb.modular.pos.pojo.Pos;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统职位表 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface PosService extends IService<Pos> {

}
