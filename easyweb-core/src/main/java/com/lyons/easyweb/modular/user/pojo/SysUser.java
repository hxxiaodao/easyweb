package com.lyons.easyweb.modular.user.pojo;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author HengDuCms
 * @since 2021-06-27
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
public class SysUser extends Model<SysUser> {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 姓名
	 */
	@NotBlank(message = "姓名,不能够为空 !")
	@TableField("real_name")
	private String realName;

	/**
	 * 昵称
	 */
	@NotBlank(message = "昵称,不能够为空 !")
	@TableField("nick_name")
	private String nickName;

	/**
	 * 账号
	 */
	@NotBlank(message = "登录账号,不能够为空 !")
	@TableField("account")
	private String account;

	/**
	 * 密码
	 */
	@TableField("password")
	private String password;

	/**
	 * 盐
	 */
	@TableField("salt")
	private String salt;

	/**
	 * 头像，存的为文件id
	 */
	@TableField("avatar")
	private String avatar;

	/**
	 * 生日
	 */
	@TableField("birthday")
	private Date birthday;

	/**
	 * 性别：0-男，1-女
	 */
	@TableField("sex")
	private Integer sex;

	/**
	 * 邮箱
	 */
	@TableField("email")
	private String email;

	/**
	 * 手机
	 */
	@TableField("phone")
	private String phone;

	/**
	 * 电话
	 */
	@TableField("tel")
	private String tel;

	/**
	 * 是否是超级管理员：1-是，0-否
	 */
	@TableField("super_admin_flag")
	private Integer superAdminFlag;

	/**
	 * 最后登陆IP
	 */
	@TableField("last_login_ip")
	private String lastLoginIp;

	/**
	 * 最后登陆时间
	 */
	@TableField("last_login_time")
	private Date lastLoginTime;
	
	/**
	 * 排序
	 */
	private Integer sort;
	
	/**
	 * 描述
	 */
	private String remark;

	/**
	 * 删除标记：0-已删除，1-未删除
	 */
	@TableField("del_flag")
	private Integer delFlag;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 更新人
	 */
	@TableField("update_user")
	private String updateUser;

	/**
	 * 状态[0-禁用;1-激活;]
	 */
	@TableField("status")
	private Integer status;

}
