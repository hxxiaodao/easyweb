package com.lyons.easyweb.modular.setting.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyons.easyweb.core.util.RedisOperator;
import com.lyons.easyweb.modular.setting.dao.SettingMapper;
import com.lyons.easyweb.modular.setting.pojo.Setting;
import com.lyons.easyweb.modular.setting.service.SettingService;

/**
 * <p>
 * 系统设置 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-05
 */
@Service
public class SettingServiceImpl extends ServiceImpl<SettingMapper, Setting> implements SettingService {

	@Autowired
	private RedisOperator redis;

	@Override
	public Setting querySettingInfo() {

		String key = "system:setting";

		if (redis.exists(key)) {
			String json = redis.get(key);
			return JSON.parseObject(json, Setting.class);
		}

		Setting setting = getOne(new QueryWrapper<Setting>().last("LIMIT 1"));
		if (!ObjectUtils.isEmpty(setting)) {

			redis.set(key, JSON.toJSONString(setting), SettingService.class.getSimpleName());
		}

		return setting;
	}

}
