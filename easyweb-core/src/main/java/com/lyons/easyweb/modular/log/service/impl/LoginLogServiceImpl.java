package com.lyons.easyweb.modular.log.service.impl;

import com.lyons.easyweb.modular.log.pojo.LoginLog;
import com.lyons.easyweb.modular.log.dao.LoginLogMapper;
import com.lyons.easyweb.modular.log.service.LoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统访问日志表 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLog> implements LoginLogService {

}
