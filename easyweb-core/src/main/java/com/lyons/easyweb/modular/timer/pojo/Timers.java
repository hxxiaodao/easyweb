package com.lyons.easyweb.modular.timer.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 定时任务
 * </p>
 *
 * @author Lyons
 * @since 2021-07-12
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_timers")
public class Timers extends Model<Timers> {

	/**
	 * 定时器ID
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 任务名称
	 */
	@TableField("timer_name")
	private String timerName;

	/**
	 * 执行任务的class的类名（实现了TimerTaskRunner接口的类的全称）
	 */
	@TableField("action_class")
	private String actionClass;

	/**
	 * 定时任务表达式
	 */
	@TableField("cron")
	private String cron;

	/**
	 * 状态（ 1运行， 2停止）
	 */
	@TableField("job_status")
	private Integer jobStatus;

	/**
	 * 备注信息
	 */
	@TableField("remark")
	private String remark;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 更新人
	 */
	@TableField("update_user")
	private String updateUser;

}
