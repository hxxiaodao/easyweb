package com.lyons.easyweb.modular.dict.dao;

import com.lyons.easyweb.modular.dict.pojo.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface DictMapper extends BaseMapper<Dict> {

}
