package com.lyons.easyweb.modular.menu.dao;

import com.lyons.easyweb.modular.menu.pojo.Menu;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单管理 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface MenuMapper extends BaseMapper<Menu> {

	/**
	 * 根据用户ID查询菜单信息
	 * 
	 * @param userId
	 * @return
	 */
	List<Menu> queryRoleMenuByUserId(@Param("userId") String userId);

}
