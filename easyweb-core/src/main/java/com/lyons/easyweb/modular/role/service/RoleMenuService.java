package com.lyons.easyweb.modular.role.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyons.easyweb.modular.role.pojo.RoleMenu;

/**
 * <p>
 * 角色与菜单对应关系 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface RoleMenuService extends IService<RoleMenu> {

	/**
	 * 保存角色选中的菜单数据
	 * 
	 * @param roleMenu
	 */
	void insertRoleMenu(RoleMenu roleMenu);

}
