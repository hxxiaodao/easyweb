package com.lyons.easyweb.modular.gener.service.impl;

import com.lyons.easyweb.modular.gener.pojo.CodeGenerateConfig;
import com.lyons.easyweb.modular.gener.dao.CodeGenerateConfigMapper;
import com.lyons.easyweb.modular.gener.service.CodeGenerateConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 代码生成详细配置 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class CodeGenerateConfigServiceImpl extends ServiceImpl<CodeGenerateConfigMapper, CodeGenerateConfig> implements CodeGenerateConfigService {

}
