package com.lyons.easyweb.modular.gener.dao;

import com.lyons.easyweb.modular.gener.pojo.CodeGenerate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 代码生成基础配置 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface CodeGenerateMapper extends BaseMapper<CodeGenerate> {

}
