package com.lyons.easyweb.modular.dept.pojo;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 部门信息
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dept")
public class Dept extends Model<Dept> {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 父级id
	 */
	@TableField(value = "pid", insertStrategy = FieldStrategy.NOT_EMPTY, updateStrategy = FieldStrategy.NOT_EMPTY)
	private String pid;

	/**
	 * 父ID串
	 */
	@TableField("pids")
	private String pids;

	/**
	 * 部门编号
	 */
	@NotBlank(message = "部门编号不能够为空 !")
	@TableField("dept_no")
	private String deptNo;

	/**
	 * 部门名称
	 */
	@NotBlank(message = "部门名，不能够为空 !")
	@TableField("name")
	private String name;

	/**
	 * 排序,大靠前
	 */
	@TableField("sort")
	private Integer sort;

	/**
	 * 部门经理user_id
	 */
	@TableField("dept_manager_id")
	private String deptManagerId;

	/**
	 * 部门经理名称
	 */
	@TableField("manager_name")
	private String managerName;

	/**
	 * 部门经理联系电话
	 */
	@TableField("phone")
	private String phone;
	
	
	private String remark;

	/**
	 * 是否删除，Y-被删除，N-未删除
	 */
	@TableField("del_flag")
	private String delFlag;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 创建用户id
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 修改时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 修改用户id
	 */
	@TableField("update_user")
	private String updateUser;

	/**
	 * 状态[0-禁用;1-激活;]
	 */
	@TableField("status")
	private Integer status;


}
