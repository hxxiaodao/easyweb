package com.lyons.easyweb.modular.menu.pojo;

import java.io.Serializable;

/*
 * zTree 树形菜单使用 - 用户授权使用
 */
@SuppressWarnings("serial")
public class TreeModel implements Serializable {

	private String id;
	private String pId;
	private String name;
	private boolean checked = false;

	public TreeModel() {
	}

	public TreeModel(String id, String pid, String name) {
		this.id = id;
		this.pId = pid;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

}
