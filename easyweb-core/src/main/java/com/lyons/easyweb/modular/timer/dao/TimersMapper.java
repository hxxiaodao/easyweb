package com.lyons.easyweb.modular.timer.dao;

import com.lyons.easyweb.modular.timer.pojo.Timers;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-12
 */
public interface TimersMapper extends BaseMapper<Timers> {

}
