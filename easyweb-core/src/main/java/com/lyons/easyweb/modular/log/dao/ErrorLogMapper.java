package com.lyons.easyweb.modular.log.dao;

import com.lyons.easyweb.modular.log.pojo.ErrorLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 错误信息 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface ErrorLogMapper extends BaseMapper<ErrorLog> {

}
