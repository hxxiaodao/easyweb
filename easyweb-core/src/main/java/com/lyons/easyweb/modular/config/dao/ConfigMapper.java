package com.lyons.easyweb.modular.config.dao;

import com.lyons.easyweb.modular.config.pojo.Config;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 参数配置 Mapper 接口
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface ConfigMapper extends BaseMapper<Config> {

}
