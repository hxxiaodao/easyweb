package com.lyons.easyweb.modular.dept.service.impl;

import com.lyons.easyweb.modular.dept.pojo.Dept;
import com.lyons.easyweb.modular.dept.dao.DeptMapper;
import com.lyons.easyweb.modular.dept.service.DeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门信息 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements DeptService {

}
