package com.lyons.easyweb.modular.log.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统访问日志表
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_login_log")
public class LoginLog extends Model<LoginLog> {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 访问账号
	 */
	@TableField("account")
	private String account;

	/**
	 * 操作类型[ 1登入 2登出]
	 */
	@TableField("vis_type")
	private Integer visType;

	/**
	 * 是否执行成功[Y-是，N-否]
	 */
	@TableField("success")
	private String success;

	/**
	 * 具体消息
	 */
	@TableField("message")
	private String message;

	/**
	 * ip
	 */
	@TableField("ip")
	private String ip;

	/**
	 * 地址
	 */
	@TableField("location")
	private String location;

	/**
	 * 浏览器
	 */
	@TableField("browser")
	private String browser;

	/**
	 * 操作系统
	 */
	@TableField("os")
	private String os;

	/**
	 * 访问时间
	 */
	@TableField("vis_time")
	private Date visTime;

}
