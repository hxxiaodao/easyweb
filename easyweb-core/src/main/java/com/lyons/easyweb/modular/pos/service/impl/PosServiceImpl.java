package com.lyons.easyweb.modular.pos.service.impl;

import com.lyons.easyweb.modular.pos.pojo.Pos;
import com.lyons.easyweb.modular.pos.dao.PosMapper;
import com.lyons.easyweb.modular.pos.service.PosService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统职位表 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class PosServiceImpl extends ServiceImpl<PosMapper, Pos> implements PosService {

}
