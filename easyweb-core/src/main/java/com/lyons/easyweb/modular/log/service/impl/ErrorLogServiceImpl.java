package com.lyons.easyweb.modular.log.service.impl;

import com.lyons.easyweb.modular.log.pojo.ErrorLog;
import com.lyons.easyweb.modular.log.dao.ErrorLogMapper;
import com.lyons.easyweb.modular.log.service.ErrorLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 错误信息 服务实现类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@Service
public class ErrorLogServiceImpl extends ServiceImpl<ErrorLogMapper, ErrorLog> implements ErrorLogService {

}
