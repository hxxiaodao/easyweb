package com.lyons.easyweb.modular.log.service;

import com.lyons.easyweb.modular.log.pojo.LoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统访问日志表 服务类
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
public interface LoginLogService extends IService<LoginLog> {

}
