package com.lyons.easyweb.modular.config.pojo;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 参数配置
 * </p>
 *
 * @author Lyons
 * @since 2021-07-01
 */
@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("sys_config")
public class Config extends Model<Config> {

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private String id;

	/**
	 * 常量所属分类的编码
	 */
	@TableField("group_code")
	private String groupCode;

	/**
	 * 是否是系统参数：Y-是，N-否
	 */
	@TableField("sys_flag")
	private String sysFlag;

	/**
	 * 名称
	 */
	@NotBlank(message = "参数名称,不能够为空 !")
	@TableField("config_name")
	private String configName;

	/**
	 * 编码
	 */
	@NotBlank(message = "参数编码,不能够为空")
	@TableField("config_code")
	private String configCode;

	/**
	 * 属性值
	 */
	@NotBlank(message = "参数值,不能够为空")
	@TableField("config_value")
	private String configValue;

	/**
	 * 排序
	 */
	@TableField("sort")
	private Integer sort;

	/**
	 * 备注
	 */
	@TableField("remark")
	private String remark;

	/**
	 * 是否删除，Y-被删除，N-未删除
	 */
	@TableField("del_flag")
	private String delFlag;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 创建用户id
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 修改时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 修改用户id
	 */
	@TableField("update_user")
	private String updateUser;

	/**
	 * 状态[0-禁用;1-激活;]
	 */
	@TableField("status")
	private Integer status;


}
