package com.lyons.easyweb.modular.notice.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyons.easyweb.modular.notice.dao.NoticeMapper;
import com.lyons.easyweb.modular.notice.pojo.Notice;
import com.lyons.easyweb.modular.notice.service.NoticeService;

/**
 * <p>
 * 通知表 服务实现类
 * </p>
 *
 * @author HengDuCms
 * @since 2021-06-28
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

	@Override
	public void save(String title, String content) {
		Notice notice = Notice.builder().title(title).content(content).createTime(new Date()).build();
		this.save(notice);
	}

	@Override
	public void save(String title, String content, String suId) {
		Notice notice = Notice.builder().title(title).suid(suId).content(content).createTime(new Date()).build();
		this.save(notice);
	}

}
