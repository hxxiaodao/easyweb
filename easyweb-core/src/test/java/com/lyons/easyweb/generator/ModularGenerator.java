package com.lyons.easyweb.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.FileOutConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

public class ModularGenerator {

	public static void main(String[] args) {
//		String tablePrefix = "url_";
//		String modularName = "website";
//		String tables = "url_server,url_server_group,url_server_local,url_website,url_website_banner,url_website_category,url_website_extr_linke,url_website_platform";

//		String tablePrefix = "";
//		String modularName = "seo";
//		String tables = "seo_category,seo_platform";

		String tablePrefix = "sys_";
		String modularName = "timer";
		String tables = "sys_timers";

		// 代码生成器
		AutoGenerator mpg = new AutoGenerator();

		// 1. 全局配置
		GlobalConfig gc = new GlobalConfig();
		gc.setAuthor("Lyons");// 作者
		gc.setOutputDir(System.getProperty("user.dir") + "/src/main/java");// 生成路径
		gc.setFileOverride(false); // 文件覆盖
		// 设置生成的service接口的名字的首字母是否为I(默认Service类前面有一个I)
		gc.setServiceName("%sService");
		gc.setIdType(IdType.ASSIGN_ID);
		gc.setDateType(DateType.ONLY_DATE);// 时间类型
		gc.setBaseResultMap(true);// 基本ResultMap
		gc.setOpen(false);
//		gc.setSwagger2(true);
		gc.setActiveRecord(true); // 是否支持AR模式
		gc.setBaseColumnList(true);// 基本的列
//		gc.setEnableCache(true);// 是否在xml中添加二级缓存配置

		mpg.setGlobalConfig(gc);

		// 数据源配置
		DataSourceConfig dsc = new DataSourceConfig();
		dsc.setUrl("jdbc:mysql://127.0.0.1:3306/hengdu-cms?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=Asia/Shanghai");
		dsc.setDriverName("com.mysql.cj.jdbc.Driver");
		dsc.setUsername("root");
		dsc.setPassword("root");
		dsc.setDbType(DbType.MYSQL);// 数据库的类型
		mpg.setDataSource(dsc);

		// 包配置
		PackageConfig pc = new PackageConfig();
//		pc.setModuleName(scanner("模块名"));
		pc.setModuleName(modularName);
		pc.setParent("com.lyons.easyweb.modular");
		pc.setController("controller");
		pc.setEntity("pojo");
		pc.setService("service");
		pc.setMapper("dao");
		pc.setXml("dao.mapping");
		mpg.setPackageInfo(pc);

		// 自定义配置
		InjectionConfig cfg = new InjectionConfig() {
			@Override
			public void initMap() {
			}
		};

		// 如果模板引擎是 freemarker
		String templatePath = "/templates/mapper.xml.ftl";

		// 自定义输出配置
		List<FileOutConfig> focList = new ArrayList<>();
		// 自定义配置会被优先输出
		focList.add(new FileOutConfig(templatePath) {
			@Override
			public String outputFile(TableInfo tableInfo) {
				// 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
				return System.getProperty("user.dir") + "/src/main/resources/mapper/" + pc.getModuleName() + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
			}
		});

		cfg.setFileOutConfigList(focList);
		mpg.setCfg(cfg);

		// 配置模板
		TemplateConfig templateConfig = new TemplateConfig();
		templateConfig.setXml(null);
		mpg.setTemplate(templateConfig);

		// 策略配置
		StrategyConfig strategy = new StrategyConfig();
		// 要生成的表名，想要生成哪个表的代码就填表名，可传多个参数，","隔开【注释掉就自动创建所有的表】
		// strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
		strategy.setInclude(tables.split(","));
		strategy.setNaming(NamingStrategy.underline_to_camel); // 下划线转驼峰命名
		strategy.setColumnNaming(NamingStrategy.underline_to_camel); // 列的名字下划线转驼峰
		strategy.setEntityLombokModel(true);
		// strategy.setLogicDeleteFieldName("deleted");//逻辑删除（deleted表明）
		strategy.setRestControllerStyle(true);
		strategy.setTablePrefix(tablePrefix);
		strategy.setControllerMappingHyphenStyle(true);
		strategy.setTablePrefix(pc.getModuleName() + "_");
		strategy.setEntityTableFieldAnnotationEnable(true);// 属性都有对应列的注释
		mpg.setStrategy(strategy);
		mpg.setTemplateEngine(new FreemarkerTemplateEngine());

		mpg.execute();
	}

	@Deprecated
	@SuppressWarnings("resource")
	public static String scanner(String tip) {
		Scanner scanner = new Scanner(System.in);
		StringBuilder help = new StringBuilder();
		help.append("请输入" + tip + "：");
		System.out.println(help.toString());
		if (scanner.hasNext()) {
			String ipt = scanner.next();
			if (StringUtils.isNotBlank(ipt)) {
				return ipt;
			}
		}
		throw new MybatisPlusException("请输入正确的" + tip + "！");
	}
}